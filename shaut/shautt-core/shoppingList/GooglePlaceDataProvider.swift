//
//  GooglePlaceDataProvider.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 08/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import GooglePlaces

class GooglePlaceDataProvider {

    private var placesTask: URLSessionDataTask?
    private var session: URLSession {
      return URLSession.shared
    }
    private var placesClient: GMSPlacesClient {
        return GMSPlacesClient.shared()
    }
    
    func fetchPostalCodeLocation(coordinate: CLLocationCoordinate2D, completionHandler: @escaping ([AddressComponentsDTO]) -> Void) {
        
        let urlString = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(coordinate.latitude),\(coordinate.longitude)&key=\(googleApiKey)"
        
        guard let url = URL(string: urlString) else {
            return
        }
        
        let request = URLRequest(url: url)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data else {
                print(String(describing: error))
                return
            }
            do {
                let resultsAddressComponents = try JSONDecoder().decode(AddressComponentsDTO.Response.self, from: data)
                if resultsAddressComponents.results.isEmpty {
                    return
                }
                completionHandler(resultsAddressComponents.results[0].addressComonents)
            } catch {
                print("ERRROR Getting all the places")
                print(error.localizedDescription)
            }
            
        }
        task.resume()
    }
}
