//
//  ComparatorViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 12/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit
import Firebase

class ComparatorViewController: UIViewController {

    @IBOutlet weak var storeProdCollection: UICollectionView!
    
    @IBOutlet weak var addButton: UIButton!
    
    var interactor: ComparatorViewToPresenterInteractorProtocol?
    var comparatorPresenter: ShoppingListComparatorViewToPresenter?
    
    var productType: ProductType?
    var storeProductsDto = [StoreProductDto]()
    var shauttSpinner: ShauttSpinnerView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        addButton.layer.cornerRadius = 23
        
        shauttSpinner = ShauttSpinnerView()
        
        view.addSubview(shauttSpinner!)
        
        shauttSpinner?.center = self.view.center
         
        shauttSpinner!.animate(shauttSpinner!.circle1, counter: 1)
        shauttSpinner!.animate(shauttSpinner!.circle2, counter: 3)
    }

    func displayAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.interactor?.getScannerOperative()
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func backToLastView(_ sender: Any) {
        self.interactor?.getScannerOperative()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addProductType(_ sender: Any) {
        Analytics.logEvent(ShauttConstants.ADD_PRODUCT_TO_SHOPPING_LIST, parameters: ["view" : "comparator"])
        self.interactor?.addProductType(productType: productType)
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ComparatorViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return storeProductsDto.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoreProductCell", for: indexPath) as! StoreProductCollectionViewCell
        
        guard !storeProductsDto.isEmpty else {
            return collectionCell
        }
        
        collectionCell.contentView.layer.borderColor = UIColor.lightGray.cgColor
        collectionCell.contentView.layer.borderWidth = 1
        collectionCell.contentView.layer.cornerRadius = 15
        
        let storeProduct = storeProductsDto[indexPath.row]
        if let url = URL(string: storeProduct.product.imageUrl) {
            collectionCell.productImage.load(url: url)
        }
        collectionCell.productName.text = storeProduct.product.name
        collectionCell.storeImage.load(path: "stores/\(storeProduct.store.name).png")
        
        collectionCell.productPrice.text = "\(storeProduct.product.price.round()) €"
        
        return collectionCell
    }
    
}

extension ComparatorViewController: ComparatorPresenterToViewProtocol {
    
    func showStoreProducts(storeProductType: StoreProductTypesDto) {
        DispatchQueue.main.async {
            self.shauttSpinner?.removeFromSuperview()
            self.storeProductsDto = storeProductType.products
            self.productType = storeProductType.productType
            self.storeProdCollection.reloadData()
        }
        
    }
    
    func drawAlert() {
        DispatchQueue.main.async {
            self.displayAlert(title: "Error", message: "Disculpa, ha habido un problema. Prueba más tarde, por favor.")
        }
    }
    
}
