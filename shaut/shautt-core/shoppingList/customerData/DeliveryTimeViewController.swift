//
//  DeliveryTimeViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 08/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit
import Firebase
import Mixpanel

class DeliveryTimeViewController: UIViewController {

    @IBOutlet weak var dayTable: UITableView!
    @IBOutlet weak var timeTable: UITableView!
    
    var cartsId = [String]()
    var timeDeliveryInteractor: DeliveryTimeDataViewToPresenterInteractorProtocol?
    
    var schedule = [ScheduleDeliveryTimeDto]()
    
    var selectedDay = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        viewSetUp(view: timeTable)
        
        setUpInteractorWithTimeView()
        self.timeDeliveryInteractor?.retrieveTimeDelivery()
    }
    
    fileprivate func viewSetUp(view: UIView) {
        view.layer.cornerRadius = 21
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        view.layer.shadowOpacity = 1
    }

    fileprivate func setUpInteractorWithTimeView() {
        let timeViewInteractor = timeDeliveryInteractor as? MasterCartPresenterInteractor
        timeViewInteractor?.timeView = self
    }
    
    func displayAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func backToView(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension DeliveryTimeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if schedule.isEmpty {
            return 1
        }
        
        if tableView == dayTable {
            return schedule.count
        } else {
            return schedule[selectedDay].deliveryHours.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == dayTable {
            let dayCell = tableView.dequeueReusableCell(withIdentifier: "DayDeliveryCell", for: indexPath) as! DayDeliveryTableViewCell
            
            if schedule.isEmpty {
                return dayCell
            }
            
            let day = schedule[indexPath.row]
            
            dayCell.dayNameLabel.text = day.dayOfWeek
            dayCell.dateDayLabel.text = day.day
            
            if selectedDay == indexPath.row {
                dayCell.contentView.layer.borderColor = UIColor.systemPink.cgColor
                dayCell.contentView.layer.borderWidth = 3
                dayCell.contentView.layer.cornerRadius = 15
            }
            
            return dayCell
        } else {
            let hourCell = tableView.dequeueReusableCell(withIdentifier: "HourDeliveryCell", for: indexPath) as! TimeDeliveryTableViewCell
            
            if schedule.isEmpty {
                return hourCell
            }
            
            let hour = schedule[selectedDay].deliveryHours[indexPath.row]
            
            if let time = hour.time, let feeCost = hour.amount {
                let stringArr = time.components(separatedBy: "-")
                hourCell.hourLabel.text = stringArr[stringArr.count - 1]
                let fee = feeCost / 100
                hourCell.feeCostLabel.text = "\(fee.round()) €"
                hourCell.feeCostLabel.textColor = .systemGreen
            }
            
            return hourCell
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == dayTable {
            let oldSelectedDayCell = tableView.cellForRow(at: IndexPath(row: selectedDay, section: 0)) as! DayDeliveryTableViewCell
            oldSelectedDayCell.contentView.layer.borderColor = .none
            oldSelectedDayCell.contentView.layer.borderWidth = 0
            oldSelectedDayCell.contentView.layer.cornerRadius = 0
            
            selectedDay = indexPath.row
            
            let dayCell = tableView.cellForRow(at: indexPath) as! DayDeliveryTableViewCell
            dayCell.contentView.layer.borderColor = UIColor.systemPink.cgColor
            dayCell.contentView.layer.borderWidth = 3
            dayCell.contentView.layer.cornerRadius = 15
            Analytics.logEvent(ShauttConstants.GET_SHOPPING_CART_DAY_TIME, parameters: nil)
            Mixpanel.mainInstance().track(event: ShauttConstants.CLICK_DELIVER_DAY_OPTION, properties: ["row": indexPath.row])
            timeTable.reloadData()
        } else if tableView == timeTable {
            Analytics.logEvent(ShauttConstants.ADD_SHOPPING_CART_DELIVERY_TIME, parameters: nil)
            Mixpanel.mainInstance().track(event: ShauttConstants.CONFIRM_DAY_HOUR_DELIVERY_TIME, properties: ["time": schedule[selectedDay].deliveryHours[indexPath.row].time ?? "unknown", "amount" : "\(schedule[selectedDay].deliveryHours[indexPath.row].amount ?? 0)"])
            self.timeDeliveryInteractor?.storeSelectedTimeDelivery(scheduleDay: schedule[selectedDay].deliveryHours[indexPath.row])
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension DeliveryTimeViewController: TimeDeliveryDataPresenterToViewProtocol {
    
    func updateTimeDelivery(schedule: [ScheduleDeliveryTimeDto]) {
        DispatchQueue.main.async {
            self.schedule = schedule
            self.dayTable.reloadData()
            self.timeTable.reloadData()
        }
    }
    
    func displayTimeDeliveryError() {
        DispatchQueue.main.async {
            self.view.isUserInteractionEnabled = true
            let message = "Lo sentimos pero ha habido un problema obteniendo el horario disponible de entrega. Pruebalo más tarde. Muchas gracias!"
            self.displayAlert(title: "Error", message: message)
        }
    }
}
