//
//  CategoryProductTypes.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 26/09/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct ResponseCategoryProductTypesDTO: Codable {
    
    var results: [CategoryProductTypesDTO]
    
}

struct CategoryProductTypesDTO: Codable {
    
    let name: String
    var items: [ProductType]
    var isExpanded: Bool?
    
}

struct ProductType: Codable, Equatable {
    
    let itemId: String
    let nameEn: String
    let nameEs: String
    let categoryId: String
    let dietType: String?
    var quantity: Int?
    
    enum CodingKeys: String, CodingKey {
        case itemId
        case nameEn = "name"
        case nameEs = "name_es"
        case categoryId
        case dietType
    }
    
    static func ==(lhs: ProductType, rhs: ProductType) -> Bool {
        return lhs.itemId == rhs.itemId
    }
}
