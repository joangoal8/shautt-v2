//
//  Decimal + Extensions.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 06/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

extension Decimal {
    
    func round(nChars: Int = 6) -> String {
        var cutDecimal = String("\(self)".prefix(nChars))
        let valueD = Double(cutDecimal)
        if let value : Double = valueD {
            cutDecimal = String(format: "%.2f", value)
        }
        return cutDecimal
    }
}
