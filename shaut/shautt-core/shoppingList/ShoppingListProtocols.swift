//
//  ShoppingListProtocols.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 01/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

protocol ShoppingListViewToPresenterInteractorProtocol: class {
    
    func retrieveShoppingList()
    
    func removeItemFromShoppingList(productType: ProductType)
    
    func editItemShoppingList(productType: ProductType)
}

protocol ShoppingListPresenterToViewProtocol: class {
    
    func drawShoppingListTable(items: [ProductType])
}

protocol ShoppingListComparatorPresenterToViewProtocol: class {
    
    func showStoreProducts(storeProductType : StoreProductTypesDto)
    
    func drawAlert()
}

protocol ShoppingListEntityToPresenterInteractorProtocol {
    
    func addItemsAndPresentShoppingListTable(items: [ProductType])
   
}
