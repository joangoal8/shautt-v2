//
//  ShoppingCartPresenterInteractor.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 07/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import KeychainSwift

class ShoppingCartPresenterInteractor {
    
    weak var shoppingCartView: ShoppingCartPresenterToViewProtocol?
    
    var shoppingCart: ShoppingCartCandidateDto?
    
    var shoppingCartEngine: ShoppingCartEngine?
    
    var keyChainSwift: KeychainSwift?
    
    init(_ shoppingViewController: ShoppingCartPresenterToViewProtocol) {
        self.shoppingCartView = shoppingViewController
    }
    
    private func getTotalAmountShoppingCart() -> Decimal {
        guard let shoppingCartDto = shoppingCart else {
            return 0
        }
        return shoppingCartDto.shoppingCartProducts.reduce(0) { (sum, shoppingCartProductType) -> Decimal in
            return sum + Decimal(shoppingCartProductType.quantity) * shoppingCartProductType.selectedProduct.price
        }
    }
    
    fileprivate func getShoppingCartEngine() -> ShoppingCartEngine {
        if let shoppingCartEngineProvider = shoppingCartEngine {
            return shoppingCartEngineProvider
        }
        shoppingCartEngine = ShoppingCartEngine(self)
        return shoppingCartEngine!
    }
    
}

extension ShoppingCartPresenterInteractor: ShoppingCartViewToPresenterInteractorProtocol {
    
    func storeShoppingCart(shoppingCart: ShoppingCartCandidateDto) {
        self.shoppingCart = shoppingCart
    }
    
    func createMasterCart(shoppingCart: ShoppingCartCandidateDto) {
        if let postcode = KeychainSwift().get("postal_code") {
        
            let masterCartItems = shoppingCart.shoppingCartProducts.map { (shoppingCartProductType) -> MasterCartItemDto in
                return MasterCartItemDto(itemId: shoppingCartProductType.selectedProduct.id, productType: shoppingCartProductType.selectedProduct.productTypeCalculated, providerItemId: shoppingCartProductType.selectedProduct.providerItemId, providerName: shoppingCartProductType.selectedProduct.providerName,  quantity: shoppingCartProductType.quantity)
            }
            
            let storeIds = shoppingCart.stores.map { (store) -> String in
                return store.providerId
            }
            
            getShoppingCartEngine().createMasterCart(postcode: postcode, storeIds: storeIds, masterCartItems: masterCartItems, provider: shoppingCart.provider)
        }
    }
}

extension ShoppingCartPresenterInteractor: ShoppingCartEntityToPresenterInteractorProtocol {
    
    func provideMasterCart(masterCart: MasterCartDto) {
        self.shoppingCartView?.provideMasterCart(masterCart: masterCart)
    }
    
    func presentError() {
        self.shoppingCartView?.displayError()
    }
    
}

extension ShoppingCartPresenterInteractor: EditShoppingCartViewToPresenterInteractorProtocol {
    
    func updateShoppingCartProducts(shoppingCart: ShoppingCartCandidateDto) {
        self.shoppingCart = shoppingCart
        let updatedAmount = getTotalAmountShoppingCart()
        self.shoppingCart?.totalAmount = updatedAmount
        shoppingCartView?.updateShoppingCartView(shoppingCart: self.shoppingCart!)
    }

}

extension ShoppingCartPresenterInteractor: EditSingleShoppingCartViewToPresenterInteractorProtocol {
    
    func updateShoppingCartProduct(shoppingCartProduct: ShoppingCartProductTypeDto, shoppingCartProductIndex: Int) {
        print("update single")
        
        shoppingCart?.shoppingCartProducts[shoppingCartProductIndex] = shoppingCartProduct
        let updatedAmount = getTotalAmountShoppingCart()
        shoppingCart?.totalAmount = updatedAmount
        shoppingCartView?.updateShoppingCartView(shoppingCart: shoppingCart!)
    }
    
}
