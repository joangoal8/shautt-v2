//
//  RegisterViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 20/07/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit
import Firebase
import Mixpanel

class RegisterViewController: UIViewController {

    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    
    let userInitConfigurationManager = UserInitConfigurationManager()
    
    @IBAction func register(_ sender: Any) {
        Mixpanel.mainInstance().track(event: ShauttConstants.CLICK_REGISTER_BUTTON)
        if emailText.text == "" || passwordText.text == "" {
            let message = "Por favor, rellena email y contraseña"
            displayRegisterAlert(message: message)
        } else {
            Auth.auth().createUser(withEmail: emailText.text!, password: passwordText.text!) {
                (authResult, error) in
                if error == nil {
                    
                    guard let auth = authResult else {
                        print("error getting the AuthResult")
                        return
                    }
                    self.userInitConfigurationManager.configureUserWithShoppingBasket(userId: auth.user.uid, email: self.emailText.text!, auth: self.passwordText.text!, providerLogin: .PasswordEmail)
                    Mixpanel.mainInstance().identify(distinctId: auth.user.uid)
                    let slackMessage = "New user is registered: \(self.emailText.text!) \n IOS"
                    SlackMessageManager.shared.sendSlackMessage(channel: SlackChannel.REGISTER, message: slackMessage)
                    self.switchToMenuViewController()
                } else if let resErr = error {
                    print("$$$$ ERROR Specified \(resErr.localizedDescription)")
                    self.displayRegisterAlert(message: resErr.localizedDescription)
                }
            }
        }
    }
    
    fileprivate func displayRegisterAlert(message: String) {
        let alertController = UIAlertController(title: "Error Registro", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        Mixpanel.mainInstance().track(event: ShauttConstants.ERROR_USER_REGISTER)
        self.present(alertController, animated: true, completion: nil)
    }
    
    fileprivate func switchToMenuViewController() {
        let onboardingViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "OnboardingController")
        onboardingViewController.modalPresentationStyle = .fullScreen
        present(onboardingViewController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Mixpanel.mainInstance().track(event: ShauttConstants.SHOW_REGISTER_VIEW)
        registerButton.layer.cornerRadius = registerButton.layer.frame.height/2
        registerButton.layer.masksToBounds = true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

}

extension RegisterViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
