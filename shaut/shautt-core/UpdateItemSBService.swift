//
//  AddItemSBService.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 28/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import Firebase

class UpdateItemSBService {
    
    let functions = Functions.functions(region : "europe-west6")
    
    weak var updateItemProductItemDelegate: UpdateItemProductItemDelegate?
    weak var updateItemToShoppingListDelegate: UpdateItemToShoppingListDelegate?
    
    private func updateItemToSB(productType: ProductType, shoppingBasketId : String, updateRequest: UpdateItemRequest) {
        guard let updateItemRequestData = try? JSONEncoder().encode(updateRequest) else {
            return
        }
        
        guard let dictionaryData = try? JSONSerialization.jsonObject(with: updateItemRequestData, options: .allowFragments) as? [String: Any] else {
            return
        }
        functions.httpsCallable("updateItemSB").call(dictionaryData) { (result, error) in
            if let err = error as NSError? {
                if err.domain == FunctionsErrorDomain {
                    let code = FunctionsErrorCode(rawValue: err.code)
                    let message = err.localizedDescription
                    let details = err.userInfo[FunctionsErrorDetailsKey]
                    print(code!)
                    print(message)
                    if let detail = details {
                        print(detail)
                    }
                }
                print(err.localizedDescription)
                return
            }
            if let data = (result?.data as? Array<Dictionary<String, Any>>){
                var shopListItems = [ProductType]()
                data.forEach { (dictProd) in
                    let itemId = dictProd["itemId"] as! String
                    let name = dictProd["name"] as! String
                    let nameEs = dictProd["name_es"] as! String
                    let categoryId = dictProd["categoryId"] as! String
                    let quantity = dictProd["quantity"] as? Int
                    var dietType: String? = nil
                    if let diet = dictProd["dietType"] {
                        dietType = diet as? String
                    }
                    shopListItems.append(ProductType(itemId: itemId, nameEn: name, nameEs: nameEs, categoryId: categoryId, dietType: dietType, quantity: quantity))
                }
                
                if let addItemInProductsDelegate = self.updateItemProductItemDelegate {
                    addItemInProductsDelegate.updateItemToShoppingBasket(productType: productType)
                }
                if let addItemToShoppingBasketDelegate = self.updateItemToShoppingListDelegate {
                    addItemToShoppingBasketDelegate.updateShoppingBasket(items: shopListItems)
                }
            }
        }
    }
    
}

extension UpdateItemSBService: AddItemProtocolService {
    
    func addItemToSB(productType: ProductType, shoppingBasketId : String) {
        let addItemRequest = UpdateItemRequest(id: shoppingBasketId, itemInfo: ItemInfo(itemId: productType.itemId, quantity: productType.quantity!), operationInfo: OperationInfo(operationAction: OperationAction.ADD))
        self.updateItemToSB(productType: productType, shoppingBasketId: shoppingBasketId, updateRequest: addItemRequest)
    }
}

extension UpdateItemSBService: RemoveItemProtocolService {
    
    func removeItemToSB(productType: ProductType, shoppingBasketId : String) {
        let removeItemRequest = UpdateItemRequest(id: shoppingBasketId, itemInfo: ItemInfo(itemId: productType.itemId, quantity: productType.quantity!), operationInfo: OperationInfo(operationAction: OperationAction.REMOVE))
        self.updateItemToSB(productType: productType, shoppingBasketId: shoppingBasketId, updateRequest: removeItemRequest)
    }
}

extension UpdateItemSBService: EditItemProtocolService {
    
    func editItemToSB(productType: ProductType, shoppingBasketId : String) {
        let newItemInfo = ItemInfo(itemId: productType.itemId, quantity: productType.quantity!)
        var operationInfo = OperationInfo(operationAction: OperationAction.EDIT)
        operationInfo.newItemInfo = newItemInfo
        let editItemRequest = UpdateItemRequest(id: shoppingBasketId, itemInfo: ItemInfo(itemId: productType.itemId, quantity: productType.quantity!), operationInfo: operationInfo)
        
        self.updateItemToSB(productType: productType, shoppingBasketId: shoppingBasketId, updateRequest: editItemRequest)
    }
}
