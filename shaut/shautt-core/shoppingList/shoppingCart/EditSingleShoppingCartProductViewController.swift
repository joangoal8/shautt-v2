//
//  EditSingleShoppingCartProductViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 06/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit
import Mixpanel

class EditSingleShoppingCartProductViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var shoppingCartProductCollection: UICollectionView!
    @IBOutlet weak var amountLabel: UILabel!
    
    var shoppingCartProductType: ShoppingCartProductTypeDto?
    var shoppingCartProductIndex: Int?
    var shoppingCartPresenterInteractor: EditSingleShoppingCartViewToPresenterInteractorProtocol?
    
    var otherOptionProductIndex: IndexPath = IndexPath(row: 0, section: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        shoppingCartProductCollection.register(UINib(nibName: "EditShoppingCartProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ShoppingCartProductCell")
        
        self.view.backgroundColor = UIColor.gray.withAlphaComponent(0.4)
        self.view.frame = UIScreen.main.bounds
        
        self.containerView.layer.borderColor = UIColor.lightGray.cgColor
        self.containerView.layer.cornerRadius = 21
        if let product = shoppingCartProductType {
            self.amountLabel.text = "\(product.quantity)"
        }
        
    }
    
    func commonSetup(shoppingCartProductType: ShoppingCartProductTypeDto, index: Int) {
        self.shoppingCartProductType = shoppingCartProductType
        self.shoppingCartProductIndex = index
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @objc fileprivate func animateIn() {
        self.containerView.transform = CGAffineTransform(translationX: 0, y: +self.view.frame.height)
        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.8, options: .curveEaseIn, animations: {
            self.containerView.transform = .identity
        })
    }
    
    @objc fileprivate func animateOut() {
        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.8, options: .curveEaseIn, animations: {
            self.containerView.transform = CGAffineTransform(translationX: 0, y: +self.view.frame.height)
        }) { (complete) in
            if complete {
                self.view.removeFromSuperview()
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func close(_ sender: Any) {
        Mixpanel.mainInstance().track(event: ShauttConstants.CLOSE_POPOUP_UPDATE_SINGLE_SHOPPING_CART_PRODUCT)
        animateOut()
    }
    
    @IBAction func updateShoppingCart(_ sender: Any) {
        guard var shoppingCartProduct = shoppingCartProductType, let shopProductIndex = shoppingCartProductIndex else {
            animateOut()
            return
        }
        shoppingCartProduct.selectedProduct = shoppingCartProduct.otherOptionsProducts[otherOptionProductIndex.row]
        Mixpanel.mainInstance().track(event: ShauttConstants.CONFIRM_UPDATE_SINGLE_SHOPPING_CART_PRODUCT, properties: ["product": shoppingCartProduct.selectedProduct.name, "quantity" : shoppingCartProduct.quantity])
        shoppingCartPresenterInteractor?.updateShoppingCartProduct(shoppingCartProduct: shoppingCartProduct, shoppingCartProductIndex: shopProductIndex)
        animateOut()
    }
    
    @IBAction func minusOne(_ sender: Any) {
        Mixpanel.mainInstance().track(event: ShauttConstants.CLICK_DECREASE_QUANTITY_SINGLE_SHOPPING_CART_PRODUCT)
        guard var shoppingCartProduct = shoppingCartProductType else {
            return
        }
        var quantity = shoppingCartProduct.quantity
        if quantity > 1 {
            quantity -= 1
            shoppingCartProduct.quantity = quantity
            shoppingCartProductType = shoppingCartProduct
            amountLabel.text = "\(quantity)"
        }
    }
    
    @IBAction func plusOne(_ sender: Any) {
        Mixpanel.mainInstance().track(event: ShauttConstants.CLICK_INCREMENT_SINGLE_SHOPPING_CART_PRODUCT)
        guard var shoppingCartProduct = shoppingCartProductType else {
            return
        }
        var quantity = shoppingCartProduct.quantity
        quantity += 1
        shoppingCartProduct.quantity = quantity
        shoppingCartProductType = shoppingCartProduct
        amountLabel.text = "\(quantity)"
    }
}

extension EditSingleShoppingCartProductViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let shopProdType = shoppingCartProductType else {
            return 1
        }
        return shopProdType.otherOptionsProducts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let prodCell = shoppingCartProductCollection.dequeueReusableCell(withReuseIdentifier: "ShoppingCartProductCell", for: indexPath) as! EditShoppingCartProductCollectionViewCell
        
        guard let shopProdType = shoppingCartProductType else {
            return prodCell
        }
        
        guard !shopProdType.otherOptionsProducts.isEmpty else {
            return prodCell
        }
        
        prodCell.containerView.layer.borderColor = prodCell.isSelected ? UIColor.systemPink.cgColor : UIColor.lightGray.cgColor
        prodCell.containerView.layer.borderWidth = prodCell.isSelected ? 3 : 1
        prodCell.containerView.layer.cornerRadius = 21
        
        let product = shopProdType.otherOptionsProducts[indexPath.row]
        if let url = URL(string: product.imageUrl) {
            prodCell.shoppingCartProductImage.load(url: url)
        }
        prodCell.shoppingCartProductName.text = product.name
        prodCell.shoppingCartProductName.lineBreakMode = .byWordWrapping
        prodCell.shoppingCartProductName.numberOfLines = 5
        
        prodCell.shoppingCartProductPrice.text = "\(product.price.round()) €"
        
        return prodCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == otherOptionProductIndex.row {
            collectionView.deselectItem(at: otherOptionProductIndex, animated: true)
            let oldProdCell = collectionView.cellForItem(at: otherOptionProductIndex) as! EditShoppingCartProductCollectionViewCell
            oldProdCell.containerView.layer.borderColor = UIColor.lightGray.cgColor
            oldProdCell.containerView.layer.borderWidth = 1
        }
        
        otherOptionProductIndex = indexPath
        
        let prodCell = collectionView.cellForItem(at: indexPath) as! EditShoppingCartProductCollectionViewCell
        
        prodCell.containerView.layer.borderColor = UIColor.systemPink.cgColor
        prodCell.containerView.layer.borderWidth = 3
    }
}
