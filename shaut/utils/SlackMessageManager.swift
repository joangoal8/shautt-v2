//
//  SlackMessageManager.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 7/3/21.
//  Copyright © 2021 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

class SlackMessageManager {
    
    private let registerChannelWebHook = "https://hooks.slack.com/services/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"
    private let appsEventsChannelWebHook = "https://hooks.slack.com/services/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"
    private let searchChannelWebHook = "https://hooks.slack.com/services/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"
    private let shoppingCartConfirmChannelWebHook = "https://hooks.slack.com/services/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"
    private let ordersChannelWebHook = "https://hooks.slack.com/services/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"
    
    static let shared: SlackMessageManager = {
        return SlackMessageManager()
    }()
    
    func sendSlackMessage(channel: SlackChannel, message: String) {
        var urlString = ""
        switch channel {
        case .REGISTER:
            urlString = registerChannelWebHook
        case .SEARCH:
            urlString = searchChannelWebHook
        case .CONFIRM:
            urlString = shoppingCartConfirmChannelWebHook
        case .ORDER:
            urlString = ordersChannelWebHook
        }
        
        guard let url = URL(string: urlString) else {
            return
        }
        
        var params: String = ""
        
        let encoder = JSONEncoder()
        let messageRequest = SlackMessage(text: message)
        
        if let jsonData = try? encoder.encode(messageRequest) {
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                params = jsonString
            }
        }
        
        let postData = params.data(using: .utf8)
        
        var request = URLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.httpMethod = "POST"
        request.httpBody = postData
            
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let err = error {
                print(err.localizedDescription)
                return
            }
        }
        task.resume()
    }
    
}

enum SlackChannel: String {
    case REGISTER, SEARCH, CONFIRM, ORDER
}

struct SlackMessage: Codable {
    let text : String
}
