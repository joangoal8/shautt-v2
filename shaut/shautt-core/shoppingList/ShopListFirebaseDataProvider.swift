//
//  ShopListFirebaseDataProvider.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 01/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import Firebase
import KeychainSwift

class ShopListFirebaseDataProvider {

    let dataBase = Firestore.firestore()
    let storage = Storage.storage()
    let functions = Functions.functions(region : "europe-west6")
    let shopListEntityToPresenterInteractorProtocol: ShoppingListEntityToPresenterInteractorProtocol
    
    init(_ tableViewPresenterInteractor: ShoppingListEntityToPresenterInteractorProtocol) {
        self.shopListEntityToPresenterInteractorProtocol = tableViewPresenterInteractor
    }
    
    func retrieveShoppingList() {
        if let shoppingBasketId = KeychainSwift().get(ShauttConstants.KEYCHAIN_SHOOPINGBASKETID_KEY) {
            retrieveShopListByShoppingBasketId(shoppingBasketId)
        } else {
            if let dataUser = KeychainSwift().getData("current_user") {
                do {
                    let decoder = JSONDecoder()
                    let user = try decoder.decode(UserLogin.self, from: dataUser)
                    retrieveShopListByUserEmail(id: user.id)
                } catch {
                    print(error)
                }
            }
        }
    }
    
    fileprivate func retrieveShopListByUserEmail(id: String) {
        let userRef = dataBase.collection("users").document(id)
        userRef.getDocument { (docSnapshot, error) in
            if let shoppingBasketID = docSnapshot?.get("shoppingBasketId") {
                KeychainSwift().set(shoppingBasketID as! String, forKey: ShauttConstants.KEYCHAIN_SHOOPINGBASKETID_KEY)
                self.retrieveShopListByShoppingBasketId(shoppingBasketID as! String)
            }
        }
    }
    
    fileprivate func retrieveShopListByShoppingBasketId(_ shoppingBasketID: String) {
        functions.httpsCallable("getShoppingBasket").call(["id" : shoppingBasketID]) { (result, error) in
            if let err = error as NSError? {
                if err.domain == FunctionsErrorDomain {
                    let code = FunctionsErrorCode(rawValue: err.code)
                    let message = err.localizedDescription
                    let details = err.userInfo[FunctionsErrorDetailsKey]
                    print(code!)
                    print(message)
                    if let detail = details {
                        print(detail)
                    }
                }
                print(err.localizedDescription)
                return
            }
            if let data = (result?.data as? Array<Dictionary<String, Any>>){
                var shopListItems = [ProductType]()
                data.forEach { (item) in
                    let itemId = item["itemId"] as! String
                    let name = item["name"] as! String
                    let nameEs = item["name_es"] as! String
                    let categoryId = item["categoryId"] as! String
                    let quantity = item["quantity"] as? Int
                    var dietType: String? = nil
                    if let diet = item["dietType"] {
                        dietType = diet as? String
                    }
                    shopListItems.append(ProductType(itemId: itemId, nameEn: name, nameEs: nameEs, categoryId: categoryId, dietType: dietType, quantity: quantity))
                }
                self.shopListEntityToPresenterInteractorProtocol.addItemsAndPresentShoppingListTable(items: shopListItems)
            }
        }
    }
}
