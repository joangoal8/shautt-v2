//
//  SignInViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 19/07/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit
import GoogleSignIn
import Firebase
import AuthenticationServices
import CryptoKit
import Mixpanel

class SignInViewController: UIViewController {
    
    @IBOutlet weak var googleSignInButton: GIDSignInButton!
    
    @IBOutlet weak var notRegisteredLabel: UILabel!
    @IBOutlet weak var logInButton: UIButton!
    
    @IBOutlet weak var appleSigninBtn: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    
    private var currentNonce: String?
    
    @IBAction func loginUser(_ sender: Any) {
        Mixpanel.mainInstance().track(event: ShauttConstants.CLICK_EMAIL_PWD_LOGIN)
        if emailText.text == "" || passwordText.text == "" {
            displayRegisterAlert(message: "Please fill an Email and password")
        } else {
            Auth.auth().signIn(withEmail: emailText.text!, password: passwordText.text!) { [weak self] authResult, error in
                guard let strongSelf = self else { return }
                if error == nil {
                    guard let auth = authResult else {
                        print("error getting the AuthResult")
                        return
                    }
                    UserInitConfigurationManager().configureLoginUserConfiguration(userId: auth.user.uid, email: strongSelf.emailText.text!, auth: strongSelf.passwordText.text!, providerLogin: .PasswordEmail)
                    strongSelf.switchToMenuViewController()
                } else if let resErr = error {
                    print("$$$$ ERROR Specified \(resErr.localizedDescription)")
                    strongSelf.displayRegisterAlert(message: resErr.localizedDescription)
                }
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerButton.layer.cornerRadius = registerButton.layer.frame.height/2
        registerButton.layer.masksToBounds = true
        
        viewSetUp(view: appleSigninBtn)
        
        logInButton.layer.cornerRadius = logInButton.layer.frame.height/2
        logInButton.layer.borderColor = UIColor.red.cgColor
        logInButton.layer.borderWidth = 1
        
        // Do any additional setup after loading the view.
        GIDSignIn.sharedInstance()?.presentingViewController = self
    
        //Remove navigation bar borders
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
    }
    
    fileprivate func viewSetUp(view: UIView) {
        view.layer.cornerRadius = 3
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        view.layer.shadowOpacity = 1
    }
    
    fileprivate func displayRegisterAlert(message: String) {
        let alertController = UIAlertController(title: "Error Login", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        Mixpanel.mainInstance().track(event: ShauttConstants.ERROR_USER_LOGIN)
        self.present(alertController, animated: true, completion: nil)
    }
    
    fileprivate func switchToMenuViewController() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainTabBarController = storyboard.instantiateViewController(identifier: "MenuTabBarController")
               
        // This is to get the SceneDelegate object from your view controller
        // then call the change root view controller function to change to main tab bar
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // Adapted from https://auth0.com/docs/api-auth/tutorials/nonce#generate-a-cryptographically-random-nonce
    private func randomNonceString(length: Int = 32) -> String {
      precondition(length > 0)
      let charset: Array<Character> =
          Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
      var result = ""
      var remainingLength = length

      while remainingLength > 0 {
        let randoms: [UInt8] = (0 ..< 16).map { _ in
          var random: UInt8 = 0
          let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
          if errorCode != errSecSuccess {
            fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
          }
          return random
        }

        randoms.forEach { random in
          if remainingLength == 0 {
            return
          }

          if random < charset.count {
            result.append(charset[Int(random)])
            remainingLength -= 1
          }
        }
      }

      return result
    }
    
    private func sha256(_ input: String) -> String {
      let inputData = Data(input.utf8)
      let hashedData = SHA256.hash(data: inputData)
      let hashString = hashedData.compactMap {
        return String(format: "%02x", $0)
      }.joined()

      return hashString
    }
    
    @IBAction func signinApple(_ sender: Any) {
        Mixpanel.mainInstance().track(event: ShauttConstants.CLICK_SOCIAL_APPLE_LOGIN)
        currentNonce = randomNonceString()
        let appleIdProvider = ASAuthorizationAppleIDProvider()
        let request = appleIdProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        request.nonce = sha256(currentNonce!)
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
    @IBAction func enterAsAGuest(_ sender: Any) {
        UserInitConfigurationManager().createShoppingBasketForAGuest { (error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            self.switchToMenuViewController()
        }
    }
    
}

extension SignInViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension SignInViewController: ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let nonce = currentNonce, let appleIdCredential = authorization.credential as? ASAuthorizationAppleIDCredential, let appleIdToken = appleIdCredential.identityToken, let appleIdTokenStr = String(data: appleIdToken, encoding: .utf8) {
            
            let credential = OAuthProvider.credential(withProviderID: "apple.com", idToken: appleIdTokenStr, rawNonce: nonce)
            
            Auth.auth().signIn(with: credential) { (authResult, error) in
                if let error = error {
                    print("There is an error with the appleSign \(error)")
                    return
                }
                guard let userUuid = authResult?.user.uid else {
                    print("Error with userUuid")
                    return
                }
                
                self.storeUserInKeychainSwift(userUuid: userUuid, idToken: appleIdTokenStr, credential: appleIdCredential)
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let mainTabBarController = storyboard.instantiateViewController(identifier: "MenuTabBarController")
                
                Mixpanel.mainInstance().identify(distinctId: userUuid)
                // This is to get the SceneDelegate object from your view controller
                // then call the change root view controller function to change to main tab bar
                (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
            }
        }
    }
    
    fileprivate func storeUserInKeychainSwift(userUuid: String, idToken: String, credential: ASAuthorizationAppleIDCredential) {
        UserInitConfigurationManager().configureSocialUserWithShoppingBasket(userId: userUuid, email:  credential.email ?? "default@apple.com", auth: idToken, providerLogin: .Apple)
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return view.window!
    }
}
