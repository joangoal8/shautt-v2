//
//  ConfirmCardViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 05/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

class ConfirmCardViewController: UIViewController {
    
    @IBOutlet weak var handlerArea: UIView!
    @IBOutlet weak var separatorMarker: UIView!
    @IBOutlet weak var shoppingCartStoreImage: UIImageView!
    @IBOutlet weak var shoppingCartStoreImage2: UIImageView!
    @IBOutlet weak var shoppingCartStoreName: UILabel!
    @IBOutlet weak var shoppingCartPrice: UILabel!
    @IBOutlet weak var confirmButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        separatorMarker.layer.cornerRadius = 4
        separatorMarker.layer.masksToBounds = true
        
        confirmButton.layer.cornerRadius = 21
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
