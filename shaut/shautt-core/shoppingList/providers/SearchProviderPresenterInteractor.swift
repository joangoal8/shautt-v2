//
//  ProviderPresenterInteractor.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 01/09/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import KeychainSwift

class SearchProviderPresenterInteractor: SearchProviderViewToPresenterInteractorProtocol {
    
    weak var view: ProviderPresenterToViewProtocol?
    
    var shoppingCartsResult = [ShoppingCartCandidateDto]()
    var providerEngine: SearchProvidersEngine?
    
    init(_ tableViewController: ProviderPresenterToViewProtocol) {
        self.view = tableViewController
    }
    
    func searchShoppingCarts(postcode: String, shopList: [ProductType]) {
        let itemsTypesInfo = extractItemTypesInfo(shopList: shopList)
        getProviderEngine().searchShoppingCarts(postcode: postcode, itemTypes: itemsTypesInfo)
    }
    
    private func extractItemTypesInfo(shopList: [ProductType]) -> [ProductTypesInformation] {
        var itemsDeliveryInfo = [ProductTypesInformation]()
        shopList.forEach { (product) in
            itemsDeliveryInfo.append(ProductTypesInformation(productTypeName: product.nameEs, quantity: product.quantity!))
        }
        return itemsDeliveryInfo
    }
    
    fileprivate func getProviderEngine() -> SearchProvidersEngine {
        if let providerEngineDataProvider = providerEngine {
            return providerEngineDataProvider
        }
        providerEngine = SearchProvidersEngine(self)
        return providerEngine!
    }

}

extension SearchProviderPresenterInteractor: ProviderEntityToPresenterInteractorProtocol {
    
    func provideShoppingCarts(shoppingCarts: [ShoppingCartCandidateDto]) {
        self.shoppingCartsResult = shoppingCarts
        self.view?.drawShoppingCarts(shoppingCarts: self.shoppingCartsResult)
    }
    
    func presentError() {
        self.view?.alertError()
    }
    
}
