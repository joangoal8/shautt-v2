//
//  ShoppingCartDataProviderEngine.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 09/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

class ShoppingCartEngine {
    
    weak var interactor: ShoppingCartEntityToPresenterInteractorProtocol?
    
    init(_ shoppingCartInteractor: ShoppingCartEntityToPresenterInteractorProtocol) {
        self.interactor = shoppingCartInteractor
    }
    
    func createMasterCart(postcode: String, storeIds: [String], masterCartItems: [MasterCartItemDto], provider: String) {
        
        let urlString = "\(ShauttConfigService.shared.getShauttBEHostUrl())/shoppingCart"
        
        guard let url = URL(string: urlString) else {
            return
        }
        
        var params: String = ""
        
        let encoder = JSONEncoder()
        let createMasterCartRequest = MasterCartRequest(storeIds: storeIds, providerProductRequests: masterCartItems, providerName: provider)
        
        if let jsonData = try? encoder.encode(createMasterCartRequest) {
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                params = jsonString
            }
        }
        
        let postData = params.data(using: .utf8)
        
        var request = URLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(postcode, forHTTPHeaderField: "postcode")
        request.addValue(shauttKey, forHTTPHeaderField: "key")
        
        request.httpMethod = "POST"
        request.httpBody = postData
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let err = error {
                print(err.localizedDescription)
                self.interactor?.presentError()
                return
            }
            if let result = data {
                do {
                    let masterCartResponse = try JSONDecoder().decode(MasterCartDtoResponse.self, from: result)
                    self.interactor?.provideMasterCart(masterCart: masterCartResponse.result)
                } catch {
                    print(error.localizedDescription)
                    self.interactor?.presentError()
                }
            } else {
                print(String(describing: error))
                self.interactor?.presentError()
            }
        }
        task.resume()
    }
}
