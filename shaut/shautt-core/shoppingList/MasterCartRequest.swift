//
//  MasterCartRequest.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 08/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct MasterCartRequest: Codable {
    
    let storeIds: [String]
    let providerProductRequests: [MasterCartItemDto]
    let providerName: String
}
