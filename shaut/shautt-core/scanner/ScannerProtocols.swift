//
//  ScannerProtocols.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 11/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

protocol ScannerViewToPresenterInteractorProtocol: class {
    
    func getProductType(barcode: String)
    
}

protocol ComparatorViewToPresenterInteractorProtocol: class {
    
    func getScannerOperative()
    
    func addProductType(productType: ProductType?)
}

protocol ScannerPresenterToViewProtocol: class {
    
    func activateScanner()
    
}

protocol ComparatorPresenterToViewProtocol: class {
    
    func showStoreProducts(storeProductType : StoreProductTypesDto)
    
    func drawAlert()
    
}

protocol ScannerEntityToPresenterProtocol: class {
    
    func provideStoreProductsDto(storeProductType : StoreProductTypesDto)
    
    func presentError()
    
}
