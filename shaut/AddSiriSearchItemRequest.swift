//
//  AddSiriSearchItemRequest.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 14/09/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct AddSiriSearchItemRequest: Encodable {
    
    let data: AddItemSiri
    
    init(data: AddItemSiri) {
        self.data = data
    }
}

struct AddItemSiri: Encodable {
    
    let shoppingBasketId: String
    let itemName: String
    let language: String
    
    init(shoppingBasketId: String, itemName: String, language: String = "es" ) {
        self.shoppingBasketId = shoppingBasketId
        self.itemName = itemName
        self.language = language
    }
}
