//
//  OnboardingSlide.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 21/12/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

struct OnboardingSlide {
    
    let title: String
    let animationName: String
    let description: String
    let buttonColor: UIColor
    let buttonTitle: String
    
    static let onboardingSlideCollection: [OnboardingSlide] = [
        .init(title: "Paso 1", animationName: "shopping-checlist", description: "Añade tus productos a tu lista de la compra", buttonColor: .systemPink, buttonTitle: "Paso 2"),
    .init(title: "Paso 2", animationName: "analyzing", description: "Compara tu lista con los diferentes supermercados", buttonColor: .systemPink, buttonTitle: "Paso 3"),
    .init(title: "Paso 3", animationName: "mobile-shopping", description: "Elige la opción que más se adapte a tus necesidades", buttonColor: .systemPink, buttonTitle: "Paso 4"),
    .init(title: "Paso 4", animationName: "delivery-animation", description: "Recibe el pedido en tu casa", buttonColor: .systemPink, buttonTitle: "Finalizar"),
    .init(title: "Bienvenido 👋🏼😍", animationName: "watermelon", description: "Empieza a añadir productos a tu lista", buttonColor: .systemPink, buttonTitle: "Ir a mi lista")
    ]
}
