//
//  AddItemProtocol.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 28/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

protocol AddItemProtocolService: class {
    
    func addItemToSB(productType: ProductType, shoppingBasketId : String)
}

protocol RemoveItemProtocolService: class {
    
    func removeItemToSB(productType: ProductType, shoppingBasketId : String)
}

protocol EditItemProtocolService: class {
    
    func editItemToSB(productType: ProductType, shoppingBasketId : String)
}

protocol UpdateItemProductItemDelegate: class {
    
    func updateItemToShoppingBasket(productType: ProductType)
}

protocol UpdateItemToShoppingListDelegate: class {
    
    func updateShoppingBasket(items: [ProductType])
}

