//
//  OnboardingService.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 11/1/21.
//  Copyright © 2021 Joan Gómez Álvarez. All rights reserved.
//

import Firebase
import FirebaseRemoteConfig

class OnboardingService {
    
    weak var interactor: OnboardingEntityToPresenterInteractorProtocol?
    
    let referralOnboardingConfig = "show_referral_onboarding"
    
    let remoteConfig: RemoteConfig
    let dataBase = Firestore.firestore()
    
    init(_ searchInteractor: OnboardingEntityToPresenterInteractorProtocol) {
        self.interactor = searchInteractor
        
        remoteConfig = RemoteConfig.remoteConfig()
        remoteConfig.setDefaults([referralOnboardingConfig : NSNumber(true)])
    }
    
    func getRemoteConfig() {
        remoteConfig.fetchAndActivate { (status, error) in
            if status == .error, let error = error {
                print(error.localizedDescription)
                self.interactor?.retrieveIsReferralOnboarding(isReferralOnboarding: true)
                return
            }
            self.interactor?.retrieveIsReferralOnboarding(isReferralOnboarding: self.remoteConfig.configValue(forKey: self.referralOnboardingConfig).boolValue)
        }
    }
    
    func storeUserIgMention(userIg: String) {
        let now = Date()
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        dataBase.collection("referral").addDocument(data: [
            "userIg": userIg,
            "date": formatter.string(from: now)
        ]) { error in
            if let error = error {
                print("Error adding the userIg mention: \(error.localizedDescription)")
            } else {
                print("Document added with userIg: \(userIg)")
            }
            self.interactor?.presentTabBar()
        }
    }
    
}
