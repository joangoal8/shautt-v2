//
//  CardState.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 07/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

enum CardState {
    case expanded
    case collapsed
}
