//
//  ItemsProtocols.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 12/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

protocol ItemViewToPresenterInteractorProtocol: class {
        
    func retrieveCategoryItems()
    
    func updateCategoryItems(categoryItems: [CategoryProductTypesDTO])
}

protocol ItemPresenterToViewProtocol: class {
    
    func defineCategoryProducts(categories: [CategoryProductTypesDTO])

}

protocol ItemEntityToPresenterInteractorProtocol {
    
    func presentCategoryItems(categoriesItems: [CategoryProductTypesDTO])
    
}

protocol AddItemViewToPresenterInteractorProtocol: class {
    
    func addItemToShoppingBasket(productType: ProductType)
}

protocol AddItemPresenterToViewProtocol: class {
    
    func drawResult()
}

protocol AddItemEntityToPresenterInteractorProtocol: class {
    
    func updateItemToShoppingBasket(item: ItemDTO)
}
