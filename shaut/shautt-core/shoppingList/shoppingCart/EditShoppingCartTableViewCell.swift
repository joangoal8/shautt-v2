//
//  EditShoppingCartTableViewCell.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 07/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

class EditShoppingCartTableViewCell: UITableViewCell {

    @IBOutlet weak var productOptionsCollection: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        productOptionsCollection.register(UINib(nibName: "EditShoppingCartProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ShoppingCartProductCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    func setCollectionViewDataSourceDelegate(collectionDelegate: UICollectionViewDataSource & UICollectionViewDelegate, forRow row: Int) {
        productOptionsCollection.delegate = collectionDelegate
        productOptionsCollection.dataSource = collectionDelegate
        productOptionsCollection.tag = row
        productOptionsCollection.reloadData()
    }
}
