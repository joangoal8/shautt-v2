//
//  ItemsDeliveryProviderInformation.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 02/09/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct ProductTypesInformation: Codable {
    let productTypeName: String
    let quantity: Int
    
    init(productTypeName: String, quantity: Int) {
        self.productTypeName = productTypeName
        self.quantity = quantity
    }
}
