//
//  MasterCartDataProvider.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 08/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

class MasterCartDataProviderEngine {
    
    weak var interactor: MasterCartDataEntityToPresenterInteractorProtocol?
    
    init(_ masterCartInteractor: MasterCartDataEntityToPresenterInteractorProtocol) {
        self.interactor = masterCartInteractor
    }
    
    func registerMasterCart(masterCart: MasterCartDto, address: AddressContactUser, token: String) {
        
        let urlString = "\(ShauttConfigService.shared.getShauttBEHostUrl())/shoppingCart/\(masterCart.id)/address"
        
        guard let url = URL(string: urlString) else {
            return
        }
        
        var params: String = ""
        
        let encoder = JSONEncoder()
        
        let userProviderAddressRequest = UserProviderAddressRequest(name: "Home \(address.addressUser.city)", address: address.addressUser.address, addressMore: address.addressMore, city: address.addressUser.city, postalCode: address.addressUser.postalCode, phone: address.phone)
        var masterCartRegisterRequest = MasterCartRegisterAddressRequest(userProviderAddress: userProviderAddressRequest, providerProducts: masterCart.providerProducts, providerName: masterCart.providerName)
        if token != "" {
            masterCartRegisterRequest.token = token
        }
        
        if let jsonData = try? encoder.encode(masterCartRegisterRequest) {
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                params = jsonString
            }
        }
        
        let postData = params.data(using: .utf8)
        
        var request = URLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(shauttKey, forHTTPHeaderField: "key")
        
        request.httpMethod = "POST"
        request.httpBody = postData
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let err = error {
                print(err.localizedDescription)
                self.interactor?.presentAddressError()
                return
            }
            if let result = data {
                do {
                    let masterCartResponse = try JSONDecoder().decode(MasterCartDtoResponse.self, from: result)
                    self.interactor?.provideMasterCart(masterCart: masterCartResponse.result)
                } catch {
                    print(error.localizedDescription)
                    self.interactor?.presentAddressError()
                }
            } else {
                print(String(describing: error))
                self.interactor?.presentAddressError()
            }
        }
        task.resume()
    }
    
    func retrieveDeliveryTime(token: String, cartIds: [String], providerName: String) {
        
         let urlString = "\(ShauttConfigService.shared.getShauttBEHostUrl())/shoppingCart/carts/deliveryTime"
        
        guard let url = URL(string: urlString) else {
            return
        }
        
        var params: String = ""
        
        let encoder = JSONEncoder()
        let cartsTimeDeliveryRequest = CartsTimeDeliveryRequest(cartIds: cartIds, providerName: providerName)
        
        if let jsonData = try? encoder.encode(cartsTimeDeliveryRequest) {
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                params = jsonString
            }
        }
        
        let postData = params.data(using: .utf8)
        
        var request = URLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(token, forHTTPHeaderField: "Authorization")
        request.addValue(shauttKey, forHTTPHeaderField: "key")
        
        request.httpMethod = "POST"
        request.httpBody = postData
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let err = error {
                print(err.localizedDescription)
                self.interactor?.presentDeliveryTimeError()
                return
            }
            if let result = data {
                do {
                    let scheduleResponse = try JSONDecoder().decode(ScheduleDeliveryTimeDtoResponse.self, from: result)
                    self.interactor?.updateTimeDelivery(schedule: scheduleResponse.schedules)
                } catch {
                    print(error.localizedDescription)
                    self.interactor?.presentDeliveryTimeError()
                }
            } else {
                print(String(describing: error))
                self.interactor?.presentDeliveryTimeError()
            }
        }
        task.resume()
    }
    
    func confirmPayment(token: String, masterCart: MasterCartDto, selectedTime: String, paymentMethod: PaymentDataDto, userId: String?) {
        
        let urlString = "\(ShauttConfigService.shared.getShauttBEHostUrl())/shoppingCart/purchase"
        
        guard let url = URL(string: urlString) else {
            return
        }
        
        var params: String = ""
        
        let encoder = JSONEncoder()
        var purchaseRequest = PurchaseRequest(masterCartId: masterCart.id, cartIds: masterCart.carts.map({ (cart) -> String in
            return cart.id
        }), providerProducts: masterCart.providerProducts, cartDeliveryTime: CartDeliveryTime(deliveryTime: selectedTime), providerPayment: paymentMethod, providerName: masterCart.providerName)
        
        if let user = userId {
            purchaseRequest.userId = user
        }
        
        if let jsonData = try? encoder.encode(purchaseRequest) {
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                params = jsonString
            }
        }
        
        let postData = params.data(using: .utf8)
        
        var request = URLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(token, forHTTPHeaderField: "Authorization")
        request.addValue(shauttKey, forHTTPHeaderField: "key")
        
        request.httpMethod = "POST"
        request.httpBody = postData
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let err = error {
                print(err.localizedDescription)
                self.interactor?.presentConfirmationError()
                return
            }
            if let result = data {
                do {
                    let orderResponse = try JSONDecoder().decode(OrderDtoResponse.self, from: result)
                    self.interactor?.provideOrderDto(order: orderResponse.result)
                } catch {
                    print(error.localizedDescription)
                    self.interactor?.presentConfirmationError()
                }
            } else {
                print(String(describing: error))
                self.interactor?.presentConfirmationError()
            }
        }
        task.resume()
    }
}

struct CartsTimeDeliveryRequest: Codable {
    
    let cartIds: [String]
    let providerName: String
}
