//
//  OnboardingProtocols.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 11/1/21.
//  Copyright © 2021 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

protocol BasicOnboardingViewToPresenterInteractorProtocol: class {
    
    func isReferralOnboarding()
}

protocol BasicOnboardingPresenterToViewProtocol: class {
    
    func showNextOnboardingStep(isReferralOnboarding: Bool)
}

protocol ReferralOnboardingViewToPresenterInteractorProtocol : class {
    
    func storeIgUserContribution(igUser: String)
    
}

protocol ReferralOnboardingPresenterToViewProtocol: class {
    
    func goToTabBar()
}

protocol OnboardingEntityToPresenterInteractorProtocol : class {
    
    func retrieveIsReferralOnboarding(isReferralOnboarding: Bool)
    
    func presentTabBar()
}
