//
//  ShoppingCartDataProtocols.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 08/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import GooglePlaces

protocol MasterCartDataViewToPresenterInteractorProtocol: class {
    
    func setMasterCart(masterCart: MasterCartDto)
    
    func confirmMasterCart(masterCart: MasterCartDto, selectedTime: String, paymentMethod: PaymentDataDto)
    
    func getDayTimeDelivery(cartIds: [String], providerName: String)
}

protocol AddressDataViewToPresenterInteractorProtocol: class {
    
    func processPlace(place: GMSPlace);
    
    func storeAddresDetailsAndPhone(house: String, door: String, phone: String)
}

protocol DeliveryTimeDataViewToPresenterInteractorProtocol: class {
    
    func retrieveTimeDelivery()
    
    func storeSelectedTimeDelivery(scheduleDay: ScheduleDayDto)
}

protocol MasterCartDataPresenterToViewProtocol: class {
    
    func displaySpinnerLoading()
    
    func updateMasterCart(mastercart: MasterCartDto)
    
    func updateDeliveryTime(scheduleDay: ScheduleDayDto)
    
    func alertAddressError()
    
    func updateOrder(order: OrderDto)
    
    func alertConfirmationError()
}

protocol AddressDataPresenterToViewProtocol: class {
    
    func updateAddressStreet(addressUser: AddressUser)
    
    func addressUpdateDismiss()
}

protocol TimeDeliveryDataPresenterToViewProtocol: class {
    
    func updateTimeDelivery(schedule: [ScheduleDeliveryTimeDto])

    func displayTimeDeliveryError()
}

protocol MasterCartDataEntityToPresenterInteractorProtocol: class {
    
    func provideMasterCart(masterCart: MasterCartDto)
    
    func updateTimeDelivery(schedule: [ScheduleDeliveryTimeDto])
    
    func presentAddressError()
    
    func presentDeliveryTimeError()
    
    func provideOrderDto(order: OrderDto)
    
    func presentConfirmationError()
}
