//
//  InjectionEngine.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 28/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

class InjectionEngine {
    
    static let updateItemSBService = UpdateItemSBService()
    
}
