//
//  ReferralOnboardingViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 10/1/21.
//  Copyright © 2021 Joan Gómez Álvarez. All rights reserved.
//

import UIKit
import Lottie

class ReferralOnboardingViewController: UIViewController {
    
    @IBOutlet weak var referralAnimationView: AnimationView!
    @IBOutlet weak var igUserInputField: UITextField!
    @IBOutlet weak var shareButton: UIButton!
    
    var referralOnboardingPresenterInteractor : ReferralOnboardingViewToPresenterInteractorProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUp()
    }
    
    private func setUp() {
        shareButton.layer.cornerRadius = shareButton.layer.frame.height/4
        shareButton.layer.masksToBounds = true
        
        let animation = Animation.named("help-grow")
        
        referralAnimationView.animation = animation
        referralAnimationView.loopMode = .loop
        referralAnimationView.contentMode = .scaleAspectFit
        
        if !referralAnimationView.isAnimationPlaying {
            referralAnimationView.play()
        }
    }
    
    func configurePresenterInteractor(presenterInteractor: OnboardingPresenterInteractor){
        presenterInteractor.referralOnboardingView = self
        referralOnboardingPresenterInteractor = presenterInteractor
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    @IBAction func sharingWithFriends(_ sender: Any) {
        let downloadText = "Descargate Shautt la aplicación que automatiza y compara tu lista de la compra 📲 \n https://apps.apple.com/es/app/shautt/id1540642536?l=es"
        let activityController = UIActivityViewController(activityItems: [downloadText], applicationActivities: nil)
        present(activityController, animated: true)
    }
    
    @IBAction func skipReferral(_ sender: Any) {
        if let text = self.igUserInputField.text, !text.isEmpty {
            self.referralOnboardingPresenterInteractor?.storeIgUserContribution(igUser: text)
        } else {
            initAppMainFuntion()
        }
    }
    
    fileprivate func initAppMainFuntion() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let mainTabBarController = storyboard.instantiateViewController(identifier: "MenuTabBarController")
        
        // This is to get the SceneDelegate object from your view controller
        // then call the change root view controller function to change to main tab bar
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}

extension ReferralOnboardingViewController: ReferralOnboardingPresenterToViewProtocol {
    
    func goToTabBar() {
        initAppMainFuntion()
    }
    
}

extension ReferralOnboardingViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
