//
//  UserInitConfigurationManager.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 04/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import Firebase

class UserInitConfigurationManager {
    
    let dataBase = Firestore.firestore()
    let functions = Functions.functions(region : "europe-west6")
    var keyChainManager: KeyChainManager?
    let userDefaults = UserDefaults(suiteName: ShauttConstants.GROUP_ID)
    
    func configureUserWithShoppingBasket(userId: String, email: String, auth: String, providerLogin: LoginProvider) {
        addUserAndLinkNewShoppingBasket(userId)
        getKeychainManager().storeUserInKeychain(id: userId, email: email, auth: auth, providerLogin: providerLogin)
    }
    
    fileprivate func linkUserWithShoppingBasket(userId: String, shoppingBasketId: String) {
        self.dataBase.collection("users").document(userId).setData([
            "shoppingBasketId": shoppingBasketId,
            "welcomePushSent" : false
        ], merge: true) { error in
            if let err = error {
                print("Error writing document: \(err)")
            } else {
                print("Everything well configured Document added with ID:")
            }
        }
        self.getKeychainManager().storeShoppingBasketIdInKeyChain(shoppingBasketId: shoppingBasketId)
        if let defaults = self.userDefaults {
            defaults.set(shoppingBasketId, forKey: ShauttConstants.USER_DEFAULTS_SHOOPINGBASKETID_KEY)
        }
    }
    
    fileprivate func createNewShoppingBasketAndLink(_ userId: String) {
        var shoppingBasketRef: DocumentReference?
        shoppingBasketRef = dataBase.collection("shoppingBaskets").addDocument(data: [:]) { error in
            if let err = error {
                print("Error adding document: \(err)")
            } else {
                self.linkUserWithShoppingBasket(userId: userId,  shoppingBasketId: shoppingBasketRef!.documentID)
            }
        }
    }
    
    fileprivate func addUserAndLinkNewShoppingBasket(_ userId: String) {
        if let shoppingBasketId = getKeychainManager().getShoppingBasketId() {
            self.linkUserWithShoppingBasket(userId: userId,  shoppingBasketId: shoppingBasketId)
        } else {
            createNewShoppingBasketAndLink(userId)
        }
    }
    
    fileprivate func getKeychainManager() -> KeyChainManager {
        if let keyChainM = keyChainManager {
            return keyChainM
        }
        keyChainManager = KeyChainManager()
        return keyChainManager!
    }
    
    func configureSocialUserWithShoppingBasket(userId: String, email: String, auth: String, providerLogin: LoginProvider) {
        
        let userRef = dataBase.collection("users").document(userId)

        userRef.getDocument { (document, error) in
            if let document = document, document.exists {
                print("The social user is already configured with the following data data: \(String(describing: document.data()))")
                if let shoppingBasketID = document.get("shoppingBasketId") {
                    self.getKeychainManager().storeShoppingBasketIdInKeyChain(shoppingBasketId: shoppingBasketID as! String)
                }
            } else {
                self.addUserAndLinkNewShoppingBasket(userId)
            }
        }
        getKeychainManager().storeUserInKeychain(id: userId, email: email, auth: auth, providerLogin: providerLogin)
    }
    
    func configureLoginUserConfiguration(userId: String, email: String, auth: String, providerLogin: LoginProvider) {
        let userRef = dataBase.collection("users").document(userId)

        userRef.getDocument { (document, error) in
            if let errResult = error {
                print(errResult.localizedDescription)
                return
            }
            if let doc = document, let dataDict = doc.data() {
                let shoppingBasketId = dataDict["shoppingBasketId"] as! String
                self.getKeychainManager().storeShoppingBasketIdInKeyChain(shoppingBasketId: shoppingBasketId)
                if let defaults = self.userDefaults {
                    defaults.set(shoppingBasketId, forKey: ShauttConstants.USER_DEFAULTS_SHOOPINGBASKETID_KEY)
                }
            }
        }
        getKeychainManager().storeUserInKeychain(id: userId, email: email, auth: auth, providerLogin: providerLogin)
    }
    
    func createShoppingBasketForAGuest(completionHandler: @escaping (Error?) -> Void) {
        var shoppingBasketRef: DocumentReference?
        shoppingBasketRef = dataBase.collection("shoppingBaskets").addDocument(data: [:]) { error in
            if let err = error {
                print("Error adding document: \(err)")
                completionHandler(err)
                return
            } else {
                self.getKeychainManager().storeShoppingBasketIdInKeyChain(shoppingBasketId: shoppingBasketRef!.documentID)
                if let defaults = self.userDefaults {
                    defaults.set(shoppingBasketRef!.documentID, forKey: ShauttConstants.USER_DEFAULTS_SHOOPINGBASKETID_KEY)
                }
                completionHandler(nil)
            }
        }
    }
    
    func addMessagingTokenToUser(user: UserLogin, messagingToken: String) {
        self.dataBase.collection("users").document(user.id).setData([
            "iosPushNotificationToken": messagingToken
        ], merge: true) { error in
            if let err = error {
                print("Error writing document: \(err)")
                print(err.localizedDescription)
            } else {
                print("Everything well configured Document added with ID:")
            }
        }
    }
}
