//
//  OnboardingPresenterInteractor.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 11/1/21.
//  Copyright © 2021 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

class OnboardingPresenterInteractor {
    
    weak var basicOnboardingView: BasicOnboardingPresenterToViewProtocol?
    weak var referralOnboardingView: ReferralOnboardingPresenterToViewProtocol?
    
    var onboardingService : OnboardingService?
    
    fileprivate func getOnboardingService() -> OnboardingService {
        if let onboardingServiceDataProvider = onboardingService {
            return onboardingServiceDataProvider
        }
        onboardingService = OnboardingService(self)
        return onboardingService!
    }
}

extension OnboardingPresenterInteractor: BasicOnboardingViewToPresenterInteractorProtocol {
    
    func isReferralOnboarding() {
        getOnboardingService().getRemoteConfig()
    }
    
}

extension OnboardingPresenterInteractor : OnboardingEntityToPresenterInteractorProtocol {
    
    func retrieveIsReferralOnboarding(isReferralOnboarding: Bool) {
        basicOnboardingView?.showNextOnboardingStep(isReferralOnboarding: isReferralOnboarding)
    }
    
    func presentTabBar() {
        referralOnboardingView?.goToTabBar()
    }
    
}


extension OnboardingPresenterInteractor: ReferralOnboardingViewToPresenterInteractorProtocol {
    
    func storeIgUserContribution(igUser: String) {
        getOnboardingService().storeUserIgMention(userIg: igUser)
    }
    
}
