//
//  ShoppingCartViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 05/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit
import Firebase
import Mixpanel
import KeychainSwift

class ShoppingCartViewController: UIViewController {
    
    @IBOutlet weak var shoppingCartTableView: UITableView!
    
    var shoppingCart: ShoppingCartCandidateDto?
    var shoppingCartPresenterInteractor: ShoppingCartViewToPresenterInteractorProtocol?

    var confirmCardView: ConfirmCardViewController!
    
    let cardHeight: CGFloat = 225
    let cardHandleAreaHeight: CGFloat = 65
    
    var isCardVisible = true
    var nextState: CardState {
        return isCardVisible ? .expanded : .collapsed
    }
    
    var runningAnimations = [UIViewPropertyAnimator]()
    var animationProgressWhenInterrupted: CGFloat = 0
    
    var shauttSpinner: ShauttSpinnerView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupCard()
    }
    
    func setupCard() {
        confirmCardView = ConfirmCardViewController(nibName: "ConfirmCardViewController", bundle: nil)
        self.addChild(confirmCardView)
        self.view.addSubview(confirmCardView.view)
        
        confirmCardView.view.frame = CGRect(x: 0, y: self.view.frame.height - cardHeight, width: self.view.bounds.width, height: cardHeight)
        confirmCardView.view.clipsToBounds = true
        confirmCardView.view.layer.borderColor = UIColor.lightGray.cgColor
        confirmCardView.view.layer.borderWidth = 1
        confirmCardView.view.layer.cornerRadius = 21
        
        confirmCardView.confirmButton.addTarget(self, action: #selector(confirmShoppingCart), for: .touchUpInside)
        
        updateCard()
        
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handleCardPan))
        
        confirmCardView.view.addGestureRecognizer(panGestureRecognizer)
    }
    
    fileprivate func updateCard() {
        if let cart = shoppingCart {
            if cart.stores.count < 2 {
                confirmCardView.shoppingCartStoreImage.load(path: "stores/\(cart.stores[0].name).png")
                confirmCardView.shoppingCartStoreName.text = cart.stores[0].name
            } else {
                confirmCardView.shoppingCartStoreImage.load(path: "stores/\(cart.stores[0].name).png")
                confirmCardView.shoppingCartStoreImage2.load(path: "stores/\(cart.stores[1].name).png")
                confirmCardView.shoppingCartStoreName.text = "\(cart.stores[0].name) + \(cart.stores[1].name)"
            }
            confirmCardView.shoppingCartPrice.text = "\(cart.totalAmount.round()) €"
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        DispatchQueue.main.async {
            if self.shoppingCartPresenterInteractor == nil {
                self.shoppingCartPresenterInteractor = ShoppingCartPresenterInteractor(self)
            }
            
            guard let shoppingCartCandidateDto = self.shoppingCart else {
                return
            }
            
            self.shoppingCartPresenterInteractor?.storeShoppingCart(shoppingCart: shoppingCartCandidateDto)
            
            print("Reloading")
            self.shoppingCartTableView.reloadData()
        }
    }
    
    @objc
    func handleCardPan(recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
        case .began:
            //startTransition
            startInteractiveTransition(state: nextState, duration: 0.9)
        case .changed:
            // updateTransition
            updateInteractiveTransition(fractionCompleted: 0)
        case .ended:
            // continueTransition
            continueInteractiveTransition()
        default:
            break
        }
    }
    
    private func animateTransitionIfNeeded(state: CardState, duration: TimeInterval) {
        let frameAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1) {
            switch state {
            case .expanded:
                self.confirmCardView.view.frame.origin.y = self.view.frame.height - self.cardHeight
            case .collapsed:
                self.confirmCardView.view.frame.origin.y = self.view.frame.height - self.cardHandleAreaHeight
            }
        }
        
        frameAnimator.addCompletion { _ in
            self.isCardVisible.toggle()
            self.runningAnimations.removeAll()
        }
        
        frameAnimator.startAnimation()
        runningAnimations.append(frameAnimator)
    }
    
    private func startInteractiveTransition(state: CardState, duration: TimeInterval) {
        if runningAnimations.isEmpty {
            animateTransitionIfNeeded(state: state, duration: duration)
        }
        runningAnimations.forEach { (animator) in
            animator.pauseAnimation()
            animationProgressWhenInterrupted = animator.fractionComplete
        }
    }
    
    private func updateInteractiveTransition(fractionCompleted: CGFloat) {
        runningAnimations.forEach { (animator) in
             animator.fractionComplete = fractionCompleted + animationProgressWhenInterrupted
        }
    }
    
    private func continueInteractiveTransition() {
        runningAnimations.forEach { (animator) in
            animator.continueAnimation(withTimingParameters: nil, durationFactor: 0)
        }
    }
    
    @objc
    func confirmShoppingCart(recognizer: UITapGestureRecognizer) {
        if let shoppingCartDto = shoppingCart {
            shauttSpinner = ShauttSpinnerView()
            
            view.addSubview(shauttSpinner!)
            
            shauttSpinner?.center = self.view.center
             
            shauttSpinner!.animate(shauttSpinner!.circle1, counter: 1)
            shauttSpinner!.animate(shauttSpinner!.circle2, counter: 3)
            
            view.isUserInteractionEnabled = false
            
            self.shoppingCartPresenterInteractor?.createMasterCart(shoppingCart: shoppingCartDto)
        }
    }
    
    fileprivate func stopActivityIndicator() {
        self.shauttSpinner?.removeFromSuperview()
    }

    func alertError() {
        let alertController = UIAlertController(title: "Error", message: "Lo sentimos pero ha habido un problema. Pruebalo más tarde. Muchas gracias!", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.view.isUserInteractionEnabled = true
            self.stopActivityIndicator()
        }))
        self.present(alertController, animated: true, completion: nil)
    }

    @IBAction func back(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EditShoppingCartFlow" {
            Analytics.logEvent(ShauttConstants.EDIT_ALL_SHOPPING_CART_PRODUCT, parameters: nil)
            if let shoppingCartCandidate = shoppingCart {
                let shoppingCartProductsText = shoppingCartCandidate.shoppingCartProducts.reduce( "") { (productsTxt, nextProduct) -> String in
                    return "\(productsTxt), \(nextProduct.selectedProduct.name)"
                }
                Mixpanel.mainInstance().track(event: ShauttConstants.EDIT_SHOPPING_CART, properties: ["products": shoppingCartProductsText])
            }
            
            let editorView = segue.destination as! EditShoppingCartViewController
            editorView.shoppingCartDto = shoppingCart
            editorView.initMatrix()
            editorView.shoppingCartPresenterInteractor = self.shoppingCartPresenterInteractor as? EditShoppingCartViewToPresenterInteractorProtocol
        }
    }
}

extension ShoppingCartViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let products = shoppingCart?.shoppingCartProducts {
            return products.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let itemCell = tableView.dequeueReusableCell(withIdentifier: "ShoppingCartItem", for: indexPath) as! ShoppingCartProductTableViewCell
        
        let shoppingCartProduct = shoppingCart?.shoppingCartProducts[indexPath.row]
        
        if let cartProduct = shoppingCartProduct {
            itemCell.itemName.text = cartProduct.selectedProduct.name
            if let url = URL(string: cartProduct.selectedProduct.imageUrl) {
                itemCell.itemImage.load(url: url)
                itemCell.itemImage.layer.cornerRadius = 8
            }
            itemCell.itemQuantityPrice.text = "\(cartProduct.quantity) x \(cartProduct.selectedProduct.price.round()) €"
            
        }
        
        return itemCell
    }
    
}

extension ShoppingCartViewController {
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    fileprivate func editShoppingCartProduct(row: Int) {
        if let shoppingCartSelect = self.shoppingCart {
            Analytics.logEvent(ShauttConstants.EDIT_SINGLE_SHOPPING_CART_PRODUCT, parameters: nil)
            let shoppingCartProduct = shoppingCartSelect.shoppingCartProducts[row]
            Mixpanel.mainInstance().track(event: ShauttConstants.CLICK_EDIT_SINGLE_SHOPPING_CART_PRODUCT, properties: ["productName": shoppingCartProduct.selectedProduct.name, "quantity" : shoppingCartProduct.quantity])
            let editShoppingCartProductView = EditSingleShoppingCartProductViewController(nibName: "EditSingleShoppingCartProductViewController", bundle: nil)
            editShoppingCartProductView.commonSetup(shoppingCartProductType: shoppingCartProduct, index: row)
            editShoppingCartProductView.shoppingCartPresenterInteractor = self.shoppingCartPresenterInteractor as? EditSingleShoppingCartViewToPresenterInteractorProtocol
            self.present(editShoppingCartProductView, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let editAction = UIContextualAction(style: .normal, title: "Editar") { (action, rowView, handler) in
            self.editShoppingCartProduct(row: indexPath.row)
        }
        editAction.backgroundColor = .orange
        
        let swipeAction = UISwipeActionsConfiguration(actions: [editAction])
        
        return swipeAction
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.editShoppingCartProduct(row: indexPath.row)
    }
}

extension ShoppingCartViewController: ShoppingCartPresenterToViewProtocol {
    
    func updateShoppingCartView(shoppingCart: ShoppingCartCandidateDto) {
        self.shoppingCart = shoppingCart
        DispatchQueue.main.async {
            self.updateCard()
            self.shoppingCartTableView.reloadData()
        }
    }
    
    func provideMasterCart(masterCart: MasterCartDto) {
        
        DispatchQueue.main.async {
            self.stopActivityIndicator()
            self.view.isUserInteractionEnabled = true
            if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "CustomerDataView") {
                viewController.modalPresentationStyle = .fullScreen
                if let dataCustomerViewController = viewController as? CustomerDataViewController {
                    Analytics.logEvent(ShauttConstants.CONFIRM_SHOPPING_CART, parameters: nil)
                    if let shoppingCartCandidate = self.shoppingCart {
                        let shoppingCartProductsText = shoppingCartCandidate.shoppingCartProducts.reduce( "") { (productsTxt, nextProduct) -> String in
                            return "\(productsTxt)\(nextProduct.selectedProduct.name), "
                        }
                        Mixpanel.mainInstance().track(event: ShauttConstants.ACCEPT_AND_CONFIRM_SHOPPING_CART, properties: ["products": shoppingCartProductsText])
                        self.sendSlackMessage(products: shoppingCartProductsText)
                    }
                    dataCustomerViewController.shoppingCart = self.shoppingCart
                    dataCustomerViewController.masterCartDto = masterCart
                    self.present(dataCustomerViewController, animated: true, completion: nil)
                }
            }
        }
    }
    
    fileprivate func sendSlackMessage(products: String) {
        if let user = KeychainSwift().getData("current_user") {
            do {
                let decoder = JSONDecoder()
                let userLogin = try decoder.decode(UserLogin.self, from: user)
                let slackMessage = "Shopping cart products: \n \(products) \n user: \(userLogin.email) \n IOS"
                SlackMessageManager.shared.sendSlackMessage(channel: SlackChannel.CONFIRM, message: slackMessage)
            } catch {
                print(error.localizedDescription)
            }
        } else {
            let slackMessage = "Shopping cart products: \n \(products) \n user: Unknown \n IOS"
            SlackMessageManager.shared.sendSlackMessage(channel: SlackChannel.CONFIRM, message: slackMessage)
        }
    }
    
    func displayError() {
        DispatchQueue.main.async {
            self.alertError()
        }
    }
    
}
