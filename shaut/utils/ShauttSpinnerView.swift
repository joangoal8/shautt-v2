//
//  ShauttSpinnerView.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 30/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

class ShauttSpinnerView: UIView {
    
    let circle1 = UIImageView(frame: CGRect(x: -80, y: 0, width: 60, height: 60))
    let circle2 = UIImageView(frame: CGRect(x: 20, y: 0, width: 60, height: 60))
    
    let positions : [CGRect] = [CGRect(x: -70, y: 0, width: 60, height: 60), CGRect(x: -20, y: -5, width: 70, height: 70), CGRect(x: 10, y: 0, width: 60, height: 60), CGRect(x: -20, y: 5, width: 50, height: 50)]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configure() {
        translatesAutoresizingMaskIntoConstraints = false
        
        circle1.backgroundColor = UIColor.lightGray.withAlphaComponent(0.9)
        circle1.layer.cornerRadius = circle1.frame.width / 2
        circle1.layer.zPosition = 2
        circle1.alpha = 0.9
        
        circle2.backgroundColor = UIColor.systemPink.withAlphaComponent(0.9)
        circle2.layer.cornerRadius = circle1.frame.width / 2
        circle2.layer.zPosition = 1
        circle2.image = UIImage(named: "logoShauttNegative.png")
        circle2.alpha = 0.9
        
        addSubview(circle1)
        addSubview(circle2)
    }
    
    func animate(_ circle: UIView, counter: Int) {
        var counter = counter
        
        UIView.animate(withDuration: 0.7, delay: 0, options: .curveLinear, animations: {
            circle.frame = self.positions[counter]
            circle.layer.cornerRadius = circle.frame.width / 2
            if circle == self.circle1 {
                self.circle1.image = UIImage(named: "clockSandDown.png")
            }
            switch counter {
            case 1:
                if circle == self.circle1 {
                    self.circle1.layer.zPosition = 2
                    self.circle1.image = UIImage(named: "clockSandUp.png")
                }
            case 3:
                if circle == self.circle1 {
                    self.circle1.layer.zPosition = 0
                    self.circle1.image = UIImage(named: "clockSandUp.png")
                }
            default:
                break
            }
        }) { (completed) in
            switch counter {
            case 0...2:
                counter += 1
            case 3:
                counter = 0
            default:
                break
            }
            
            self.animate(circle, counter: counter)
        }
    }
}
