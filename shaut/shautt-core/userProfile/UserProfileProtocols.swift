//
//  UserProfileProtocols.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 03/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

protocol UserProfileViewToPresenterInteractorProtocol: class {
    
    func storeUserImage(userId: String, data: Data)
    
}

protocol ResetPasswordViewToPresenterInteractorProtocol: class {
    
    func resetUserPassword(email: String, password: String, newPassword: String)
}

protocol OrderViewToPresenterInteractorProtocol: class {
    
    func retrieveOrders(userId: String)
    
}

protocol UserProfilePresenterToViewProtocol: class {
 
}

protocol ResetPasswordPresenterToViewProtocol: class {
    
    func displayPasswordNotification();
    
    func displayPasswordError()
}

protocol OrderPresenterToViewProtocol: class {
    
    func displayOrders(orders: [UserOrderDto]);

}

protocol UserProfileEntityToPresenterInteractorProtocol {
    
    func passwordUpdated();
    
    func presentError();
    
    func presentOrders(orders: [UserOrderDto])
}
