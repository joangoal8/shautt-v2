//
//  UserProfilePresenterInteractor.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 03/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

class UserProfilePresenterInteractor {
    
    weak var view: UserProfilePresenterToViewProtocol?
    weak var passwordView: ResetPasswordPresenterToViewProtocol?
    weak var orderView: OrderPresenterToViewProtocol?
    
    var userFirebaseDataProvider: UserProfileDataProvider?
    var userOrdersDto = [UserOrderDto]()
    
    init(_ tableViewController: UserProfilePresenterToViewProtocol) {
        self.view = tableViewController
    }
    
    fileprivate func getUserProfileFirebaseDataProvider() -> UserProfileDataProvider {
        if let userProfileFirebaseProvider = userFirebaseDataProvider {
            return userProfileFirebaseProvider
        }
        userFirebaseDataProvider = UserProfileDataProvider(self)
        return userFirebaseDataProvider!
    }
    
}

extension UserProfilePresenterInteractor: UserProfileViewToPresenterInteractorProtocol {
    
    func storeUserImage(userId: String, data: Data) {
        getUserProfileFirebaseDataProvider().storeUserImage(userId: userId, data: data)
    }
}

extension UserProfilePresenterInteractor: ResetPasswordViewToPresenterInteractorProtocol {
    
    func resetUserPassword(email: String, password: String, newPassword: String) {
        getUserProfileFirebaseDataProvider().resetPassword(email: email , password: password, newPassword: newPassword)
    }
}

extension UserProfilePresenterInteractor: OrderViewToPresenterInteractorProtocol {
    
    func retrieveOrders(userId: String) {
           getUserProfileFirebaseDataProvider().retrieveOrders(userId: userId)
       }
}

extension UserProfilePresenterInteractor: UserProfileEntityToPresenterInteractorProtocol {
    
    func passwordUpdated() {
        self.passwordView?.displayPasswordNotification()
    }
    
    func presentError() {
        self.passwordView?.displayPasswordError()
    }
    
    func presentOrders(orders: [UserOrderDto]) {
        self.userOrdersDto = orders
        self.orderView?.displayOrders(orders: orders)
    }
}

