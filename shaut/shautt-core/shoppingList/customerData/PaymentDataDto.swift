//
//  PaymentDto.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 10/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct PaymentDataDto: Codable {
    
    let masterCart: Int
    let creditCard: String
    let creditCardExpirationMonth: Int
    let creditCardExpirationYear: Int
    let creditCardSecurity: String
}
