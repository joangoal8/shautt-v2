//
//  ProductCategoryFirestoreDataProvider.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 12/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

class ProductCategoryDataProvider {
    
    weak var productItemPresenterInteractor: ProductItemPresenterInteractor?

    init(_ tableViewPresenterInteractor: ProductItemPresenterInteractor) {
        self.productItemPresenterInteractor = tableViewPresenterInteractor
    }
    
    func retrieveCategoriesItems() {
        let urlString = "https://europe-west6-shaut-tech.cloudfunctions.net/getAllProductTypes"
        
        guard let url = URL(string: urlString) else {
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let result = data else {
                print(String(describing: error))
                return
            }
            do {
                let categoriesProducts = try JSONDecoder().decode(ResponseCategoryProductTypesDTO.self, from: result)
                self.productItemPresenterInteractor?.presentCategoryItems(categoriesItems: categoriesProducts.results)
            } catch {
                print(error.localizedDescription)
            }
            
        }
        task.resume()
    }
    
}
