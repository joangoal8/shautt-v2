//
//  PaymentCardViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 07/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

class PaymentCardViewController: UIViewController {

    @IBOutlet weak var separatorMarker: UIView!
    @IBOutlet weak var deliveryCostLabel: UILabel!
    @IBOutlet weak var cardStoreImage1: UIImageView!
    @IBOutlet weak var cardStoreImage2: UIImageView!
    @IBOutlet weak var cardStoresName: UILabel!
    @IBOutlet weak var shoppingCartPrice: UILabel!
    @IBOutlet weak var paymentButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        separatorMarker.layer.cornerRadius = 4
        separatorMarker.layer.masksToBounds = true
        
        paymentButton.layer.cornerRadius = 21
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
