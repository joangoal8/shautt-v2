//
//  ScannerPresenterInteractor.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 11/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import KeychainSwift

class ScannerPresenterInteractor {
    
    weak var view: ScannerPresenterToViewProtocol?
    weak var comparatorView: ComparatorPresenterToViewProtocol?
    
    var scannerEngine: ScannerEngine?
    
    init(_ viewController: ScannerPresenterToViewProtocol) {
        self.view = viewController
    }
    
    fileprivate func getScannerEngine() -> ScannerEngine {
        if let scannerDataProvider = scannerEngine {
            return scannerDataProvider
        }
        scannerEngine = ScannerEngine(self)
        return scannerEngine!
    }
    
}

extension ScannerPresenterInteractor: ScannerViewToPresenterInteractorProtocol {
    
    func getProductType(barcode: String) {
        self.getScannerEngine().compareProductType(barcode: barcode)
    }
}

extension ScannerPresenterInteractor: ScannerEntityToPresenterProtocol {
    
    func provideStoreProductsDto(storeProductType: StoreProductTypesDto) {
        self.comparatorView?.showStoreProducts(storeProductType: storeProductType)
    }
    
    func presentError() {
        self.comparatorView?.drawAlert()
    }
    
}

extension ScannerPresenterInteractor: ComparatorViewToPresenterInteractorProtocol{
    func addProductType(productType: ProductType?) {
        if let shoppingBasketId = KeychainSwift().get(ShauttConstants.KEYCHAIN_SHOOPINGBASKETID_KEY), var productTypeDto = productType {
            productTypeDto.quantity = 1
            let addItemProtocol = UpdateItemSBService()
            addItemProtocol.addItemToSB(productType: productTypeDto, shoppingBasketId: shoppingBasketId)
        }
        self.view?.activateScanner()
    }
    
    
    func getScannerOperative() {
        self.view?.activateScanner()
    }
    
    
}
