//
//  UserProfileViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 18/07/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit
import KeychainSwift
import Firebase

class UserProfileViewController: UIViewController, UINavigationControllerDelegate {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userEmail: UILabel!
    
    @IBOutlet weak var userDataContainer: UIView!
    @IBOutlet weak var userOrdersContainer: UIView!
    @IBOutlet weak var logoutContainer: UIView!
    
    var isProfileImageEdited = false
    var isBackGroundImageEdited = false
    var user: UserLogin?
    var userProfilePresenterInteractor: UserProfileViewToPresenterInteractorProtocol?
    
    let firebaseAuth = Auth.auth()
    let imagePickerController = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if userProfilePresenterInteractor == nil {
            userProfilePresenterInteractor = UserProfilePresenterInteractor(self)
        }
        imagePickerController.delegate = self
        
        viewSetUp(view: userDataContainer)
        viewSetUp(view: userOrdersContainer)
        viewSetUp(view: logoutContainer)
        
        updateUserInfo()
    }
    
    fileprivate func viewSetUp(view: UIView) {
        view.layer.cornerRadius = 21
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        view.layer.shadowOpacity = 1
    }
    
    fileprivate func updateUserInfo() {
        if let dataUser = KeychainSwift().getData("current_user") {
            do {
                let decoder = JSONDecoder()
                let user = try decoder.decode(UserLogin.self, from: dataUser)
                self.user = user
                let emailWithoutHost = user.email.components(separatedBy: "@")
                userEmail.text = "\(emailWithoutHost[0])"
                profileImage.layer.cornerRadius = profileImage.frame.height/2
                profileImage.load(path: "users/\(user.id).png")
            } catch {
                print(error)
                print(error.localizedDescription)
                presentLoginAndRedirect()
            }
        } else {
            presentLoginAndRedirect()
        }
    }
    
    func presentLoginAndRedirect() {
        let loginNavController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "LoginNavigationController")
        present(loginNavController, animated: true, completion: nil)
    }
    
    @IBAction func edit(_ sender: Any) {
        if self.user == nil {
            presentLoginAndRedirect()
            return
        }
        imagePickerController.sourceType = .photoLibrary
        
        imagePickerController.allowsEditing = true
        
        self.present(imagePickerController, animated: true) {
            self.isProfileImageEdited = true
        }
    }

    
    @IBAction func logout(_ sender: Any) {
        if self.user == nil {
            presentLoginAndRedirect()
            return
        }
        let alertController = UIAlertController(title: "Logout", message: "Estás seguro que quieres cerrar sesión", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        
        alertController.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { (action) in
            self.logoutAction()
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    fileprivate func logoutAction() {
        do {
            try firebaseAuth.signOut()
            KeyChainManager().cleanUserKeyChain()
            switchRootViewController()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
    fileprivate func switchRootViewController() {
        self.dismiss(animated: true, completion: nil)
        let keyWindow = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .map({$0 as? UIWindowScene})
            .compactMap({$0})
            .first?.windows
            .filter({$0.isKeyWindow}).first
        if let window = keyWindow {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let loginNavController = storyboard.instantiateViewController(identifier: "LoginNavigationController")
            window.rootViewController = loginNavController
        }
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if self.user == nil {
            presentLoginAndRedirect()
            return
        }
        if segue.identifier == "ResetPasswordFlow" {
            let resetPasswordView = segue.destination as! ResetPasswordViewController
            resetPasswordView.user = self.user
            let interactor = self.userProfilePresenterInteractor as! UserProfilePresenterInteractor
            interactor.passwordView = resetPasswordView as ResetPasswordPresenterToViewProtocol
            resetPasswordView.interactor = self.userProfilePresenterInteractor as? ResetPasswordViewToPresenterInteractorProtocol
        }
        if segue.identifier == "OrdersFlow" {
            let orderView = segue.destination as! OrdersViewController
            orderView.user = self.user
            let interactor = self.userProfilePresenterInteractor as! UserProfilePresenterInteractor
            interactor.orderView = orderView as OrderPresenterToViewProtocol
            orderView.interactor = self.userProfilePresenterInteractor as? OrderViewToPresenterInteractorProtocol
        }
    }

}

extension UserProfileViewController: UserProfilePresenterToViewProtocol {
    
}

extension UserProfileViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let imagePicked = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            if (isProfileImageEdited) {
                profileImage.image = imagePicked
                profileImage.layer.cornerRadius = profileImage.frame.height/2
                profileImage.clipsToBounds = true
                let resizedImage = imagePicked.resizeSizeCustomSize()
                if let data = resizedImage.pngData(), let user = user {
                    self.userProfilePresenterInteractor?.storeUserImage(userId: user.id, data: data)
                }
            }
        } else {
            let alertController = UIAlertController(title: "Error Image", message: "Try again later", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                self.dismiss(animated: true, completion: nil)
            }))
        }
        self.dismiss(animated: true) {
            self.isProfileImageEdited = false
            self.isBackGroundImageEdited = false
        }
    }
}
