//
//  UserProfileFirebaseDataProvider.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 04/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import Firebase

class UserProfileDataProvider {
    
    let dataBase = Firestore.firestore()
    let storage = Storage.storage()
    
    let userPresenterProtocol: UserProfileEntityToPresenterInteractorProtocol
    
    init(_ tableViewPresenterInteractor: UserProfileEntityToPresenterInteractorProtocol) {
           self.userPresenterProtocol = tableViewPresenterInteractor
    }
    
    func storeUserImage(userId: String, data: Data) {
        let userRef = storage.reference().child("users/\(userId).png")
        userRef.putData(data)
    }
    
    func resetPassword(email: String, password: String, newPassword: String) {
   
        let auth = Auth.auth()
        auth.signIn(withEmail: email, password: password) { [weak self] authResult, error in
            guard let strongSelf = self else { return }
            if let error = error {
                print("$$$$ ERROR Specified \(error.localizedDescription)")
                strongSelf.userPresenterProtocol.presentError()
                return
            }
            auth.currentUser?.updatePassword(to: newPassword) { (error) in
                if let error = error {
                    print(error.localizedDescription)
                    strongSelf.userPresenterProtocol.presentError()
                    return
                }
                strongSelf.userPresenterProtocol.passwordUpdated()
            }
        }
    }
    
    func retrieveOrders(userId: String) {
        let urlString = "\(ShauttConfigService.shared.getShauttBEHostUrl())/user/\(userId)/orders"
        
        guard let url = URL(string: urlString) else {
            return
        }
        
        var request = URLRequest(url: url)
        request.addValue(shauttKey, forHTTPHeaderField: "key")
        
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            var userOrdersDto = [UserOrderDto]()
            if let err = error {
                print(err.localizedDescription)
                return
            }
            if let result = data {
                do {
                    let userOrdersResponse = try JSONDecoder().decode(UserOrderDtoResponse.self, from: result)
                    userOrdersDto = userOrdersResponse.results
                } catch {
                    print(error.localizedDescription)
                }
            } else {
                print(String(describing: error))
            }
            self.userPresenterProtocol.presentOrders(orders: userOrdersDto)
        }
        task.resume()
    }
}
