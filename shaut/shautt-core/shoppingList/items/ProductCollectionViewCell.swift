//
//  ProductCollectionViewCell.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 09/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productButtonImage: CustomItemButton!
    
    @IBOutlet weak var imageProduct: UIImageView!
    @IBOutlet weak var nameTxt: UILabel!
    
    func setCustomTarget(_ target: Any?, action: Selector, for controlEvents: UIControl.Event, productType: ProductType?) {
        productButtonImage.productType = productType
        productButtonImage.addTarget(target, action: action, for: controlEvents)
        
        if let product = productType {
            imageProduct.load(path: "products/images/\(product.itemId).png")
        }
        
    }
}
