//
//  ComparatorEngine.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 28/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

class ComparatorEngine {
    
    weak var interactor: ComparatorEntityToInteractor?
    
    init(_ interactor: ComparatorEntityToInteractor) {
        self.interactor = interactor
    }
    
    func compareProductType(productType: ProductType) {
        let urlString = "https://europe-west6-shaut-tech.cloudfunctions.net/compareByProductType"
        
        guard let url = URL(string: urlString) else {
            return
        }
        var params: String = ""
        
        let encoder = JSONEncoder()
        let comparatorRequest = ComparatorRequest(data: ProductTypeComparator(productType: productType))
        
        if let jsonData = try? encoder.encode(comparatorRequest) {
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                params = jsonString
            }
        }
        
        let postData = params.data(using: .utf8)
        
        var request = URLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.httpMethod = "POST"
        request.httpBody = postData
            
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let err = error {
                print(err.localizedDescription)
                self.interactor?.presentError()
                return
            }
            if let result = data {
                do {
                    let storeProductsResponse = try JSONDecoder().decode(StoreProductDtoResponse.self, from: result)
                    self.interactor?.provideStoreProductsDto(storeProductType: storeProductsResponse.result)
                } catch {
                    print(error.localizedDescription)
                    self.interactor?.presentError()
                }
            } else {
                    print(String(describing: error))
                    self.interactor?.presentError()
                }
            }
        task.resume()
    }
    
}

struct ComparatorRequest: Codable {
    let data : ProductTypeComparator
}

struct ProductTypeComparator: Codable {
    let productType: ProductType
}
