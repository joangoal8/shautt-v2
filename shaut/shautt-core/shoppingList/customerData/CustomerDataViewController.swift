//
//  CustomerDataViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 07/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit
import Firebase
import Mixpanel
import KeychainSwift

class CustomerDataViewController: UIViewController {

    @IBOutlet weak var addressBtnContainer: UIView!
    @IBOutlet weak var timeBtnContainer: UIView!
    @IBOutlet weak var paymentContainer: UIView!
    
    @IBOutlet weak var addressButton: UIButton!
    @IBOutlet weak var timeButton: UIButton!
    
    @IBOutlet weak var creditCardTextField: UITextField!
    @IBOutlet weak var expirationMonthTextField: UITextField!
    @IBOutlet weak var expirationYearTextField: UITextField!
    @IBOutlet weak var cvvTextField: UITextField!
    
    var shoppingCart: ShoppingCartCandidateDto?
    var paymentCardView: PaymentCardViewController!
    var masterCartDto: MasterCartDto?
    var scheduleDay: ScheduleDayDto?
    
    var masterCartPresenterInteractor: MasterCartDataViewToPresenterInteractorProtocol?
    
    let cardHeight: CGFloat = 285
    let cardHandleAreaHeight: CGFloat = 65
    
    var isCardVisible = true
    var nextState: CardState {
        return isCardVisible ? .expanded : .collapsed
    }
    
    var runningAnimations = [UIViewPropertyAnimator]()
    var animationProgressWhenInterrupted: CGFloat = 0
    
    var shauttSpinner: ShauttSpinnerView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        viewSetUp(view: addressBtnContainer)
        viewSetUp(view: timeBtnContainer)
        viewSetUp(view: paymentContainer)
        
        textFieldCurrentSetup(textField: creditCardTextField)
        textFieldCurrentSetup(textField: expirationMonthTextField)
        textFieldCurrentSetup(textField: expirationYearTextField)
        textFieldCurrentSetup(textField: cvvTextField)
        
        setUpPaymentCard()
        
        if self.masterCartPresenterInteractor == nil {
            self.masterCartPresenterInteractor = MasterCartPresenterInteractor(self)
        }
    }
    
    fileprivate func viewSetUp(view: UIView) {
        view.layer.cornerRadius = 21
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        view.layer.shadowOpacity = 1
    }
    
    fileprivate func textFieldCurrentSetup(textField: UITextField) {
        textField.borderStyle = .none
    }
    
    func setUpPaymentCard() {
        paymentCardView = PaymentCardViewController(nibName: "PaymentCardViewController", bundle: nil)
        self.addChild(paymentCardView)
        self.view.addSubview(paymentCardView.view)
        
        paymentCardView.view.frame = CGRect(x: 0, y: self.view.frame.height - cardHeight, width: self.view.bounds.width, height: cardHeight)
        paymentCardView.view.clipsToBounds = true
        paymentCardView.view.layer.borderColor = UIColor.lightGray.cgColor
        paymentCardView.view.layer.borderWidth = 1
        paymentCardView.view.layer.cornerRadius = 21
        
        paymentCardView.paymentButton.addTarget(self, action: #selector(closeAndPayShoppingCart), for: .touchUpInside)
        
        updateCard()
        
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handleCardPan))
        
        paymentCardView.view.addGestureRecognizer(panGestureRecognizer)
    }
    
    fileprivate func updateCard() {
        if let cart = shoppingCart {
            if cart.stores.count < 2 {
                paymentCardView.cardStoreImage1.load(path: "stores/\(cart.stores[0].name).png")
                paymentCardView.cardStoresName.text = cart.stores[0].name
            } else {
                paymentCardView.cardStoreImage1.load(path: "stores/\(cart.stores[0].name).png")
                paymentCardView.cardStoreImage2.load(path: "stores/\(cart.stores[1].name).png")
                paymentCardView.cardStoresName.text = "\(cart.stores[0].name) + \(cart.stores[1].name)"
            }
            paymentCardView.shoppingCartPrice.text = "\(cart.totalAmount.round()) €"
        }
    }
    
    fileprivate func updateCostFeeCard() {
        if let scheduleDay = scheduleDay, let cart = shoppingCart, let amount = scheduleDay.amount, amount > 0 {
            let realAmount = amount / 100
            paymentCardView.deliveryCostLabel.text = "\(realAmount.round()) €"
            paymentCardView.deliveryCostLabel.textColor = .red
            paymentCardView.shoppingCartPrice.text = "\(cart.totalAmount.round()) €"
        } else {
            paymentCardView.deliveryCostLabel.text = "Free distribuido por deliberry"
        }
        paymentCardView.deliveryCostLabel.font = paymentCardView.deliveryCostLabel.font.withSize(15)
        paymentCardView.deliveryCostLabel.numberOfLines = 2
    }
    
    @objc
    func handleCardPan(recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
        case .began:
            //startTransition
            startInteractiveTransition(state: nextState, duration: 0.9)
        case .changed:
            // updateTransition
            updateInteractiveTransition(fractionCompleted: 0)
        case .ended:
            // continueTransition
            continueInteractiveTransition()
        default:
            break
        }
    }
    
    private func animateTransitionIfNeeded(state: CardState, duration: TimeInterval) {
        let frameAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1) {
            switch state {
            case .expanded:
                self.paymentCardView.view.frame.origin.y = self.view.frame.height - self.cardHeight
            case .collapsed:
                self.paymentCardView.view.frame.origin.y = self.view.frame.height - self.cardHandleAreaHeight
            }
        }
        
        frameAnimator.addCompletion { _ in
            self.isCardVisible.toggle()
            self.runningAnimations.removeAll()
        }
        
        frameAnimator.startAnimation()
        runningAnimations.append(frameAnimator)
    }
    
    private func startInteractiveTransition(state: CardState, duration: TimeInterval) {
        if runningAnimations.isEmpty {
            animateTransitionIfNeeded(state: state, duration: duration)
        }
        runningAnimations.forEach { (animator) in
            animator.pauseAnimation()
            animationProgressWhenInterrupted = animator.fractionComplete
        }
    }
    
    private func updateInteractiveTransition(fractionCompleted: CGFloat) {
        runningAnimations.forEach { (animator) in
             animator.fractionComplete = fractionCompleted + animationProgressWhenInterrupted
        }
    }
    
    private func continueInteractiveTransition() {
        runningAnimations.forEach { (animator) in
            animator.continueAnimation(withTimingParameters: nil, durationFactor: 0)
        }
    }
    
    @objc
    func closeAndPayShoppingCart(recognizer: UITapGestureRecognizer) {
        if validatingFields(), let masterCart = masterCartDto, let scheduleDay = scheduleDay, let time = scheduleDay.time {
            activateSpinner()
            guard let numberMasterCart = Int(masterCart.id), let monthNum = Int(expirationMonthTextField.text!), let yearNum = Int("20\(expirationYearTextField.text!)") else {
                let message = "Introduce los campos correctamente. Muchas gracias"
                self.displayAlert(title: "Error", message: message)
                return
            }
            Analytics.logEvent(ShauttConstants.PURCHASE_SHOPPING_CART, parameters: nil)
            Mixpanel.mainInstance().track(event: ShauttConstants.PAY_AND_CONFIRM_SHOPPING_CART)
            self.masterCartPresenterInteractor?.confirmMasterCart(masterCart: masterCart, selectedTime: time, paymentMethod: PaymentDataDto(masterCart: numberMasterCart, creditCard: creditCardTextField.text!, creditCardExpirationMonth: monthNum, creditCardExpirationYear: yearNum, creditCardSecurity: cvvTextField.text!))
        }
        
    }
    
    private func validatingFields() -> Bool {
        guard let masterCard = masterCartDto, let address = masterCard.deliveryAddress, let scheduleDay = scheduleDay, let time = scheduleDay.time else {
            let message = "Todos los campos son necesarios."
            self.displayAlert(title: "Error", message: message)
            return false
        }
        let regexCreditCardAndCVV = try? NSRegularExpression(pattern: "([0-9])", options: .caseInsensitive)
        if regexCreditCardAndCVV?.firstMatch(in: creditCardTextField.text!, options: [], range: NSRange(location: 0, length: creditCardTextField.text!.count)) == nil {
            let message = "Error con la targeta de credito proporcionada. Intente añadir tu tarjeta de crédito sin espacios ni guiones."
            self.displayAlert(title: "Error", message: message)
            return false
        }
        if regexCreditCardAndCVV?.firstMatch(in: cvvTextField.text!, options: [], range: NSRange(location: 0, length: cvvTextField.text!.count)) == nil {
            let message = "Indica el código de seguridad correctamente."
            self.displayAlert(title: "Error", message: message)
            return false
        }
        let regexMonthAndYear = try? NSRegularExpression(pattern: "([0-9]){2}", options: .caseInsensitive)
        if regexMonthAndYear?.firstMatch(in: expirationMonthTextField.text!, options: [], range: NSRange(location: 0, length: expirationMonthTextField.text!.count)) == nil {
            let message = "Indica el mes de la fecha de caducidad de la tarjeta correctamente. Ejemplo 01."
            self.displayAlert(title: "Error", message: message)
            return false
        }
        if regexMonthAndYear?.firstMatch(in: expirationYearTextField.text!, options: [], range: NSRange(location: 0, length: expirationYearTextField.text!.count)) == nil {
            let message = "Indica el año de la fecha de caducidad de la tarjeta correctamente. Ejemplo 21."
            self.displayAlert(title: "Error", message: message)
            return false
        }
        
        return address.address != "" && time != "" && creditCardTextField.text != "" && cvvTextField.text != "" && expirationMonthTextField.text != "" && expirationYearTextField.text != ""
    }
    
    fileprivate func stopActivityIndicator() {
        self.shauttSpinner?.removeFromSuperview()
    }
    
    @IBAction func back(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    func displayAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.stopActivityIndicator()
        }))
        Mixpanel.mainInstance().track(event: ShauttConstants.ERROR_CLICKING_ADD_DELIVERY_TIME)
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    private func activateSpinner() {
        shauttSpinner = ShauttSpinnerView()
        
        view.addSubview(shauttSpinner!)
        
        shauttSpinner?.center = self.view.center
         
        shauttSpinner!.animate(shauttSpinner!.circle1, counter: 1)
        shauttSpinner!.animate(shauttSpinner!.circle2, counter: 3)
        
        view.isUserInteractionEnabled = false
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddressDataFlow" {
            Analytics.logEvent(ShauttConstants.INI_ADD_ADDRESS_TO_SHOPPING_CART, parameters: nil)
            Mixpanel.mainInstance().track(event: ShauttConstants.CLICK_TO_ADD_ADDRESS)
            self.masterCartPresenterInteractor?.setMasterCart(masterCart: masterCartDto!)
            let addressDataView = segue.destination as! AddressDataViewController
            addressDataView.interactor = self.masterCartPresenterInteractor as? AddressDataViewToPresenterInteractorProtocol
        }
        if segue.identifier == "DeliveryTimeFlow" {
            if let masterCart = masterCartDto, masterCart.deliveryAddress != nil {
                let deliveryTimeView = segue.destination as! DeliveryTimeViewController
                let cardsId = masterCart.carts.map({ (cart) -> String in
                    return cart.id
                })
                deliveryTimeView.cartsId = cardsId
                Analytics.logEvent(ShauttConstants.CHECK_SHOPPING_CART_DELIVERY_TIME, parameters: nil)
                Mixpanel.mainInstance().track(event: ShauttConstants.CLICK_TO_ADD_DELIVERY_TIME, properties: ["cartsId" : cardsId])
                deliveryTimeView.timeDeliveryInteractor = self.masterCartPresenterInteractor as? DeliveryTimeDataViewToPresenterInteractorProtocol
            } else {
                displayAlert(title: "Atención", message: "Se debe indicar primero la dirección de entrega")
            }
        }
    }
    
}

extension CustomerDataViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension CustomerDataViewController: MasterCartDataPresenterToViewProtocol {
        
    func displaySpinnerLoading() {
        DispatchQueue.main.async {
            self.activateSpinner()
        }
    }
    
    func updateMasterCart(mastercart: MasterCartDto) {
        DispatchQueue.main.async {
            self.masterCartPresenterInteractor?.getDayTimeDelivery(cartIds: mastercart.carts.map({ (cart) -> String in
                return cart.id
            }), providerName: mastercart.providerName)
            self.stopActivityIndicator()
            self.view.isUserInteractionEnabled = true
            self.masterCartDto = mastercart
            if let address = mastercart.deliveryAddress {
                self.addressButton.setTitle(address.address, for: .normal)
                self.addressButton.setTitleColor(.black, for: .normal)
            }
            
        }
    }
    
    func alertAddressError() {
        DispatchQueue.main.async {
            self.stopActivityIndicator()
            self.view.isUserInteractionEnabled = true
            let message = "Lo sentimos pero ha habido un problema actualizando la dirección de entrega. Pruebalo más tarde. Muchas gracias!"
            self.displayAlert(title: "Error", message: message)
        }
    }
    
    func updateDeliveryTime(scheduleDay: ScheduleDayDto) {
        DispatchQueue.main.async {
            self.scheduleDay = scheduleDay
            self.updateCostFeeCard()
            self.timeButton.setTitle(scheduleDay.time, for: .normal)
            self.timeButton.setTitleColor(.black, for: .normal)
        }
    }
    
    func updateOrder(order: OrderDto) {
        DispatchQueue.main.async {
            self.stopActivityIndicator()
            self.view.isUserInteractionEnabled = true
            Mixpanel.mainInstance().track(event: ShauttConstants.SHOPPING_IS_CONFIRMED_SHOPPING_CART)
            self.sendSlackMessageOrder(order: order)
            if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationView") {
                viewController.modalPresentationStyle = .fullScreen
                if let confirmationView = viewController as? ConfirmationViewController {
                    confirmationView.shoppingCartDto = self.shoppingCart
                    confirmationView.orderDto = order
                    self.present(confirmationView, animated: true, completion: nil)
                }
            }
        }
    }
    
    fileprivate func sendSlackMessageOrder(order: OrderDto) {
        if let user = KeychainSwift().getData("current_user") {
            do {
                let decoder = JSONDecoder()
                let userLogin = try decoder.decode(UserLogin.self, from: user)
                let slackMessage = "Order: \n user: \(userLogin.email) \n orderId=: \(order.uuid) \n orderProviderId: \(order.providerOrderId ?? "unknown") \n amount: \(order.amount) \n stores: \(order.storeRef) \n address: \(order.address) \n city: \(order.city) \n postcode: \(order.postalCode) \n phone: \(order.phone) \n deliveryTimeHour: \(order.deliveryTime.hour) \n IOS"
                SlackMessageManager.shared.sendSlackMessage(channel: SlackChannel.ORDER, message: slackMessage)
            } catch {
                print(error.localizedDescription)
            }
        } else {
            let slackMessage = "Order: \n orderId=: \(order.uuid) \n orderProviderId: \(order.providerOrderId ?? "unknown") \n amount: \(order.amount) \n stores: \(order.storeRef) \n address: \(order.address) \n city: \(order.city) \n postcode: \(order.postalCode) \n phone: \(order.phone) \n deliveryTimeHour: \(order.deliveryTime.hour) \n IOS"
            SlackMessageManager.shared.sendSlackMessage(channel: SlackChannel.ORDER, message: slackMessage)
        }
    }
    
    func alertConfirmationError() {
        DispatchQueue.main.async {
            self.stopActivityIndicator()
            self.view.isUserInteractionEnabled = true
            let message = "Lo sentimos pero ha habido un problema con el pago. Pruebalo más tarde. Muchas gracias!"
            self.displayAlert(title: "Error", message: message)
        }
    }

}
