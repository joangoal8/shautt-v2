//
//  ProvidersViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 31/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit
import Firebase
import Mixpanel

class SearchProvidersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var providerTableView: UITableView!
    
    var providerPresenterInteractor: SearchProviderViewToPresenterInteractorProtocol?
    
    var shoppingList = [ProductType]()
    var shoppingCarts = [ShoppingCartCandidateDto]()
    var postalcode: String?
    var shauttSpinner: ShauttSpinnerView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        providerTableView.showsVerticalScrollIndicator = false
        providerTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        if providerPresenterInteractor == nil {
            self.providerPresenterInteractor = SearchProviderPresenterInteractor(self)
        }
        
        shauttSpinner = ShauttSpinnerView()
        
        view.addSubview(shauttSpinner!)
        
        shauttSpinner?.center = self.view.center
         
        shauttSpinner!.animate(shauttSpinner!.circle1, counter: 1)
        shauttSpinner!.animate(shauttSpinner!.circle2, counter: 3)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        providerPresenterInteractor?.searchShoppingCarts(postcode: postalcode!, shopList: shoppingList)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return shoppingCarts.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let providerCell = tableView.dequeueReusableCell(withIdentifier: "ProviderCell", for: indexPath) as! ProviderTableViewCell

        let shoppingCart = shoppingCarts[indexPath.row]
        
        //let double = NSDecimalNumber(decimal:shoppingCart.totalAmount).doubleValue
        //providerCell.providerPrice.text = "\(String(format: "%.2f", double)) €"
        providerCell.providerPrice.text = "\(shoppingCart.totalAmount.round()) €"
        
        if shoppingCart.stores.count < 2 {
            providerCell.providerImageView1.load(path: "stores/\(shoppingCart.stores[0].name).png")
        } else {
            providerCell.providerImageView1.load(path: "stores/\(shoppingCart.stores[0].name).png")
            providerCell.providerImageView2.load(path: "stores/\(shoppingCart.stores[1].name).png")
        }
        
        providerCell.rowContentView.layer.cornerRadius = 21
        providerCell.rowContentView.layer.borderWidth = 2
        providerCell.rowContentView.layer.borderColor = UIColor.systemPink.cgColor
        providerCell.rowContentView.layer.shadowColor = UIColor.systemPink.cgColor
        providerCell.rowContentView.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        providerCell.rowContentView.layer.shadowOpacity = 1
        
        return providerCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let shoppingCart = shoppingCarts[indexPath.row]
        
        if let viewController = storyboard?.instantiateViewController(withIdentifier: "ShoppingCartView") {
            viewController.modalPresentationStyle = .fullScreen
            if let shoppingCartViewController = viewController as? ShoppingCartViewController {
                Analytics.logEvent(ShauttConstants.CHOSE_STORE_IN_SHOPPING_LIST, parameters: nil)
                let storesText = shoppingCart.stores.reduce( "") { (storesTxt, nextStore) -> String in
                    return "\(storesTxt), \(nextStore.name)"
                }
                Mixpanel.mainInstance().track(event: ShauttConstants.CHOOSE_STORE_OPTION, properties: ["stores": storesText, "price" : "\(shoppingCart.totalAmount)"])
                shoppingCartViewController.shoppingCart = shoppingCart
                self.present(shoppingCartViewController, animated: true, completion: nil)
            }
        }
    }

    @IBAction func backToLastView(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

extension SearchProvidersViewController: ProviderPresenterToViewProtocol {
   
    func drawShoppingCarts(shoppingCarts: [ShoppingCartCandidateDto]) {
        DispatchQueue.main.async {
            self.shauttSpinner?.removeFromSuperview()
            self.shoppingCarts = shoppingCarts
            self.providerTableView.reloadData()
        }
    }
    
    func alertError() {
        print(" Alert ")
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: "No disponible", message: "Lo sentimos, pero ha habido un error. Puedes reportarlo a contact@shautt.com. Disculpe las molestias", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                self.shauttSpinner?.removeFromSuperview()
                self.dismiss(animated: true, completion: nil)
            }))
            self.present(alertController, animated: true, completion: nil)
        }
    }
}
