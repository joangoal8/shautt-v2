//
//  ShauttConfigService.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 26/12/20.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

class ShauttConfigService {
    
    private let httpProtocol = "https://"
    private var shauttBEHost = "shaut-tech.ew.r.appspot.com"
    private let coreServiceVersionPath = "/shautt-engine/v1"
    
    static let shared: ShauttConfigService = {
        return ShauttConfigService()
    }()
    
    func getShauttConfig() {
        let urlString = "https://europe-west6-shaut-tech.cloudfunctions.net/getShauttConfig"
        
        guard let url = URL(string: urlString) else {
            return
        }
        
        var request = URLRequest(url: url)
        
        request.httpMethod = "POST"
            
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let err = error {
                print(err.localizedDescription)
                return
            }
            if let result = data {
                do {
                    let configurationResponse = try JSONDecoder().decode(ShauttConfigResponse.self, from: result)
                    self.shauttBEHost = configurationResponse.host
                } catch {
                    print(error.localizedDescription)
                }
            } else {
                    print(String(describing: error))
                }
            }
        task.resume()
    }
    
    func getShauttBEHostUrl() -> String {
        return "\(httpProtocol)\(shauttBEHost)\(coreServiceVersionPath)"
    }
}
