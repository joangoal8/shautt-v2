//
//  MasterCartRegisterAddressDto.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 09/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct MasterCartRegisterAddressRequest: Codable {
    
    let userProviderAddress: UserProviderAddressRequest
    let providerProducts: [MasterCartItemDto]
    let providerName: String
    var token: String?
}

struct UserProviderAddressRequest: Codable {
    
    let name: String
    let address: String
    let addressMore: String
    let city: String
    let postalCode: String
    let phone: String
    let comments = ""
}
