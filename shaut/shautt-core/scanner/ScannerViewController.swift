//
//  ScannerViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 11/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit
import AVFoundation
import Firebase

class ScannerViewController: UIViewController {

    var captureSession = AVCaptureSession()
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    var interactor: ScannerViewToPresenterInteractorProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.interactor == nil {
            self.interactor = ScannerPresenterInteractor(self)
        }
        
        // Do any additional setup after loading the view.
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            
            guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else {
                print("Your device is not aplicable for video processing")
                return
            }
            let videoInput: AVCaptureDeviceInput
            
            do {
                videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
            } catch {
                print("Your device cannot give video input!")
                return
            }
            
            if (self.captureSession.canAddInput(videoInput)) {
                self.captureSession.addInput(videoInput)
            } else {
                print("Your device cannot add input in capture session")
                return
            }
            
            let metadataOutput = AVCaptureMetadataOutput()
            
            if (self.captureSession.canAddOutput(metadataOutput)){
                self.captureSession.addOutput(metadataOutput)
                
                metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
                metadataOutput.metadataObjectTypes = [.ean8, .ean13]
            } else {
                return
            }
            
            self.previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
            self.previewLayer.frame = self.view.layer.bounds
            self.previewLayer.videoGravity = .resizeAspectFill
            self.view.layer.addSublayer(self.previewLayer)
            print("Running")
            self.captureSession.startRunning();
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ScannerViewController: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if let first = metadataObjects.first {
            guard let readableObject = first as? AVMetadataMachineReadableCodeObject else {
                return
            }
            guard let stringValue = readableObject.stringValue else {
                return
            }
            found(code: stringValue)
        } else {
            print("Not able to read the code! Please try again")
        }
    }
    
    func found(code: String) {
        if let viewController = storyboard?.instantiateViewController(withIdentifier: "ComparatorView") {
            if let comparatorViewController = viewController as? ComparatorViewController {
                self.captureSession.stopRunning()
                Analytics.logEvent(ShauttConstants.SCAN_BARCODE_PRODUCT, parameters: ["barcode" : code])
                self.interactor?.getProductType(barcode: code)
                comparatorViewController.interactor = self.interactor as? ComparatorViewToPresenterInteractorProtocol
                let interactor = self.interactor as? ScannerPresenterInteractor
                interactor?.comparatorView = comparatorViewController
                viewController.modalPresentationStyle = .fullScreen
                self.present(comparatorViewController, animated: true, completion: nil)
            }
        }
        
    }
}

extension ScannerViewController: ScannerPresenterToViewProtocol {
    
    func activateScanner() {
        DispatchQueue.main.async {
            self.captureSession.startRunning();
        }
    }
    
}
