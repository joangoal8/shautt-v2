//
//  SiriIntentService.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 14/09/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

class SiriIntentService: AddItemBySearchNameCommandProtocolService {
    
    static let sharedInstance = SiriIntentService()
    
    func addItemByNameToSB(itemName: String, shoppingBasketId: String, completionHandler: @escaping (String?, Error?) -> Void) {
         
        let urlString = "https://europe-west6-shaut-tech.cloudfunctions.net/addItemByName"
        
        guard let url = URL(string: urlString) else {
            return
        }
        
        var params: String = ""
        
        let encoder = JSONEncoder()
        let addSiriSearchItemRequest = AddSiriSearchItemRequest(data: AddItemSiri(shoppingBasketId: shoppingBasketId, itemName: itemName))
        
        print(addSiriSearchItemRequest)
        if let jsonData = try? encoder.encode(addSiriSearchItemRequest) {
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                params = jsonString
            }
        }
        
        print(params)
        let postData = params.data(using: .utf8)
        
        var request = URLRequest(url: url)
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.httpMethod = "POST"
        request.httpBody = postData
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let result = data else {
                print(String(describing: error))
                completionHandler(nil, error)
                return
            }
            print( "The result is \(String(data: result, encoding: .utf8)!)")
            completionHandler(String(data: result, encoding: .utf8), nil)
        }
        task.resume()
     }
    
    func addMultiItemsByNameToSB(itemName: String, shoppingBasketId: String) {
        
       let urlString = "https://europe-west6-shaut-tech.cloudfunctions.net/addItemByName"
       
       guard let url = URL(string: urlString) else {
           return
       }
       
       var params: String = ""
       
       let encoder = JSONEncoder()
       let addSiriSearchItemRequest = AddSiriSearchItemRequest(data: AddItemSiri(shoppingBasketId: shoppingBasketId, itemName: itemName))
       
       print(addSiriSearchItemRequest)
       if let jsonData = try? encoder.encode(addSiriSearchItemRequest) {
           if let jsonString = String(data: jsonData, encoding: .utf8) {
               params = jsonString
           }
       }
       
       print(params)
       let postData = params.data(using: .utf8)
       
       var request = URLRequest(url: url)
       
       request.addValue("application/json", forHTTPHeaderField: "Content-Type")
       
       request.httpMethod = "POST"
       request.httpBody = postData
       
       let task = URLSession.shared.dataTask(with: request) { data, response, error in
           guard let result = data else {
               print(String(describing: error))
               return
           }
           print( "The result is \(String(data: result, encoding: .utf8)!)")
       }
       task.resume()
    }
}
