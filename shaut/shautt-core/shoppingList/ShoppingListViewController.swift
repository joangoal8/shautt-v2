//
//  ShoppingListViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 02/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Intents
import KeychainSwift
import Firebase
import FirebaseMessaging
import AppTrackingTransparency
import AdSupport
import Mixpanel

class ShoppingListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var buyNowButton: UIButton!
    @IBOutlet weak var customTableView: UITableView!
    
    private let locationManager = CLLocationManager()
    
    var shopListPresenterInteractor: ShoppingListViewToPresenterInteractorProtocol?
    var shoppingLocationPresenterInteractor: ShoppingLocationViewToPresenterInteractorProtocol?
    var shoppingList = [ProductType]()
    var totalAmountShoppingBasket: Decimal = 0
    var user: UserLogin?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if shopListPresenterInteractor == nil {
            self.shopListPresenterInteractor = ShopListPresenterInteractor(self)
        }
        
        self.shopListPresenterInteractor?.retrieveShoppingList()
        
        //Not needed for the moment
        authAddTracking()
        
        locationManager.delegate = self
        UNUserNotificationCenter.current().delegate = self
        Messaging.messaging().delegate = self
        
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.requestWhenInUseAuthorization()
            self.locationManager.startUpdatingLocation()
        }
        
        pushNotificationSetup()
        
        INPreferences.requestSiriAuthorization { (status) in
            print("Siri Authorization Status - ", status)
        }
        
        getUserInfo()
        
        navigationBar.backgroundColor = self.view.backgroundColor
        buyNowButton.layer.cornerRadius = buyNowButton.layer.frame.height/4
        buyNowButton.layer.masksToBounds = true
    }
    
    fileprivate func pushNotificationSetup() {
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: { isAuth, error in
                if let error = error {
                    print(error.localizedDescription)
                    Analytics.logEvent(ShauttConstants.ERROR_PUSH_NOTIFICATIONS, parameters: nil)
                    Mixpanel.mainInstance().track(event: ShauttConstants.ERROR_ALLOWING_PUSH_NOTIFICATIONS)
                    return
                }
                if isAuth {
                    Analytics.logEvent(ShauttConstants.ALLOW_PUSH_NOTIFICATIONS, parameters: nil)
                    Mixpanel.mainInstance().track(event: ShauttConstants.ACCEPT_PUSH_NOTIFICATIONS)
                } else {
                    Analytics.logEvent(ShauttConstants.DOES_NOT_ALLOW_PUSH_NOTIFICATIONS, parameters: nil)
                    Mixpanel.mainInstance().track(event: ShauttConstants.DOES_NOT_ACCEPT_PUSH_NOTIFICATIONS)
                }
                Messaging.messaging().subscribe(toTopic: ShauttConstants.SHAUTT_IOS_TOPIC)
                
                Messaging.messaging().delegate = self
        })
        
        UIApplication.shared.registerForRemoteNotifications()
        
    }
    
    fileprivate func authAddTracking() {
        if #available(iOS 14, *) {
            ATTrackingManager.requestTrackingAuthorization { status in
                    switch status {
                    case .authorized:
                        // Tracking authorization dialog was shown
                        // and we are authorized
                        print("Authorized")
                        
                        // Now that we are authorized we can get the IDFA
                        let idfa = ASIdentifierManager.shared().advertisingIdentifier
                        print(idfa)
                        
                    case .denied:
                        // Tracking authorization dialog was
                        // shown and permission is denied
                        print("Denied")
                    case .notDetermined:
                        // Tracking authorization dialog has not been shown
                        print("Not Determined")
                    case .restricted:
                        print("Restricted")
                    @unknown default:
                        print("Unknown")
                    }
                }
        }
    }
    
    fileprivate func getUserInfo() {
        if let dataUser = KeychainSwift().getData("current_user") {
            do {
                let decoder = JSONDecoder()
                let user = try decoder.decode(UserLogin.self, from: dataUser)
                self.user = user
            } catch {
                print(error)
                print(error.localizedDescription)
            }
        }
    }
        
    override func viewDidAppear(_ animated: Bool) {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return shoppingList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let itemCell = tableView.dequeueReusableCell(withIdentifier: "ShopItem", for: indexPath) as! ShopItemTableViewCell
        let itemDto = shoppingList[indexPath.row]

        itemCell.itemImage.load(path: "products/images/\(itemDto.itemId).png")
        itemCell.itemName.text = itemDto.nameEs
        itemCell.itemQuantity.text = "\(itemDto.quantity!)"
        
        return itemCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.editShoppingListItem(row: indexPath.row)
    }

    
    @IBAction func refreshShoppingBasket(_ sender: Any) {
        if shopListPresenterInteractor == nil {
            self.shopListPresenterInteractor = ShopListPresenterInteractor(self)
        }
        Mixpanel.mainInstance().track(event: ShauttConstants.SYNC_SHOPPING_LIST)
        self.shopListPresenterInteractor?.retrieveShoppingList()
    }
    
    func storePostalCode(location: CLLocation) {
        if shoppingLocationPresenterInteractor == nil {
            self.shoppingLocationPresenterInteractor = LocationPresenterInteractor()
        }
        
        shoppingLocationPresenterInteractor?.fetchShoppingLocation(coordinate: location.coordinate)
    }
    
    func presentLoginAndRedirect() {
        let loginNavController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "LoginNavigationController")
        present(loginNavController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ProviderView" {
            if self.user == nil {
                presentLoginAndRedirect()
                return
            }
            if shoppingList.isEmpty {
                let alertController = UIAlertController(title: "Tu lista está vacía", message: "Añade productos a tu lista de la compra para poder compararlos. Muchas gracias 🥰", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alertController, animated: true, completion: nil)
            }
            let postcode = shoppingLocationPresenterInteractor?.retrievePostalCode()
            Analytics.logEvent(ShauttConstants.CLICK_COMPARE_SHOPPING_LIST, parameters: nil)
            let shoppingListTxt = shoppingList.reduce( "") { (shoppingListText, nextProduct) -> String in
                return "\(shoppingListText), \(nextProduct.nameEs)"
            }
            Mixpanel.mainInstance().track(event: ShauttConstants.CLICK_COMPARE_BUTTON_SHOPPING_LIST, properties: ["list" : shoppingListTxt])
            if let postalCode = postcode {
                Mixpanel.mainInstance().track(event: ShauttConstants.ACCEPT_RECOGNIZED_POSTCODE, properties: ["postcode" : postalCode])
                displayConfirmLocationAlert(postCode: postalCode, for: segue)
            } else {
                self.displayInputLocationAlert(for: segue)
            }
        }
        if segue.identifier == "ItemsView" {
            Analytics.logEvent(ShauttConstants.LIST_SHOPPING_LIST_PRODUCTS, parameters: nil)
            Mixpanel.mainInstance().track(event: ShauttConstants.CHECK_ALL_CATEGORY_PRODUCTS)
        }
    }
    
    fileprivate func displayConfirmLocationAlert(postCode: String, for segue: UIStoryboardSegue) {
        let alertController = UIAlertController(title: "Indica el código Postal", message: "La aplicación ha reconocido que tu ubicación es " + postCode + ". ¿Es correcto?", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "No", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
            self.displayInputLocationAlert(for: segue)
        }))
        alertController.addAction(UIAlertAction(title: "Sí", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
            if segue.identifier == "ProviderView" {
                self.sendMetricsAndMessageCompareBE(postcode: postCode)
                let providerView = segue.destination as! SearchProvidersViewController
                providerView.shoppingList = self.shoppingList
                providerView.postalcode = postCode
                self.present(providerView, animated: true, completion: nil)
            }
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    fileprivate func sendMetricsAndMessageCompareBE(postcode: String) {
        Analytics.logEvent(ShauttConstants.COMPARE_SHOPPING_LIST, parameters: nil)
        let shoppingListTxt = self.shoppingList.reduce( "") { (shoppingListText, nextProduct) -> String in
            return "\(shoppingListText)\(nextProduct.nameEs), "
        }
        Mixpanel.mainInstance().track(event: ShauttConstants.COMPARE_SHOPPING_LIST_BE, properties: ["postcode": postcode, "list" : shoppingListTxt])
        Mixpanel.mainInstance().people.set(properties: ["$region": postcode])
        let slackMessage = "Shopping list: \n \(shoppingListTxt) \n user: \(user?.email ?? "unknown") \n postcode: \(postcode) \n IOS"
        SlackMessageManager.shared.sendSlackMessage(channel: SlackChannel.SEARCH, message: slackMessage)
    }
    
    fileprivate func displayInputLocationAlert(for segue: UIStoryboardSegue) {
        Analytics.logEvent(ShauttConstants.SHOW_EDIT_POSTCODE, parameters: nil)
        Mixpanel.mainInstance().track(event: ShauttConstants.VIEW_SET_CUSTOM_POSTCODE)
        let alertController = UIAlertController(title: "Indica el código Postal", message: "¿Podrías indicar el código postal?", preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Código postal"
        }
        alertController.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        alertController.addAction(UIAlertAction(title: "Acceptar", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
            let postcodeText = alertController.textFields![0] as UITextField
            if let postcode = postcodeText.text, segue.identifier == "ProviderView" {
                self.shoppingLocationPresenterInteractor?.storePostcodeInKeychain(postalcode: postcode)
                self.sendMetricsAndMessageCompareBE(postcode: postcode)
                let providerView = segue.destination as! SearchProvidersViewController
                providerView.shoppingList = self.shoppingList
                providerView.postalcode = postcode
                self.present(providerView, animated: true, completion: nil)
            }
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func shareShoppingList(_ sender: Any) {
        var shoppingListTxt = shoppingList.reduce( "La lista de la compra es: \n") { (shoppingListText, nextProduct) -> String in
            return "\(shoppingListText) · \(nextProduct.nameEs) x\(nextProduct.quantity ?? 1)\n"
        }
        shoppingListTxt = "\(shoppingListTxt) Descargatela en: https://apps.apple.com/es/app/shautt/id1540642536?l=es"
        Mixpanel.mainInstance().track(event: ShauttConstants.CLICK_SHARE_SHOPPING_LIST, properties: ["shareText": shoppingListTxt])
        let activityController = UIActivityViewController(activityItems: [shoppingListTxt], applicationActivities: nil)
        present(activityController, animated: true, completion: nil)
    }
    
}

extension ShoppingListViewController: ShoppingListPresenterToViewProtocol {
    
    func drawShoppingListTable(items: [ProductType]) {
        self.shoppingList = items
        self.customTableView.reloadData()
    }
    
}

extension ShoppingListViewController {
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let deleteAction = UIContextualAction(style: .destructive, title: "Borrar") { (action, rowView, handler) in
            self.shopListPresenterInteractor?.removeItemFromShoppingList(productType: self.shoppingList[indexPath.row])
            self.shoppingList.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        
        let compareAction = UIContextualAction(style: .normal, title: "Comparar") { (action, rowView, handler) in
            self.compareProductType(indexPath: indexPath)
        }
        compareAction.backgroundColor = .systemGreen
        
        let editAction = UIContextualAction(style: .normal, title: "Editar") { (action, rowView, handler) in
            self.editShoppingListItem(row: indexPath.row)
        }
        editAction.backgroundColor = .orange
        
        let swipeAction = UISwipeActionsConfiguration(actions: [deleteAction, compareAction, editAction])
        
        return swipeAction
    }
    
    fileprivate func compareProductType(indexPath: IndexPath) {
        if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ComparatorView") {
            if let comparatorViewController = viewController as? ComparatorViewController {
                let comparatorPresenter = ComparatorPresenterInteractor()
                comparatorViewController.comparatorPresenter = comparatorPresenter as ShoppingListComparatorViewToPresenter
                comparatorPresenter.comparatorView = comparatorViewController
                let shopItem = self.shoppingList[indexPath.row]
                
                let item = ProductType(itemId: shopItem.itemId, nameEn: shopItem.nameEn, nameEs: shopItem.nameEs, categoryId: shopItem.categoryId, dietType: shopItem.dietType, quantity: shopItem.quantity)
                comparatorPresenter.compareProductType(productType: item)
                viewController.modalPresentationStyle = .fullScreen
                self.present(comparatorViewController, animated: true, completion: nil)
            }
        }
    }
    
    fileprivate func editShoppingListItem(row: Int) {
        let shopItem = self.shoppingList[row]
        
        let item = ProductType(itemId: shopItem.itemId, nameEn: shopItem.nameEn, nameEs: shopItem.nameEs, categoryId: shopItem.categoryId, dietType: shopItem.dietType, quantity: shopItem.quantity)
        
        let addItemToShoppingBasketView = Bundle.main.loadNibNamed("AddItemToShoppingBasketView", owner: nil, options: nil)?[0] as? AddItemToShoppingBasketView
        
        if let newView = addItemToShoppingBasketView {
            Mixpanel.mainInstance().track(event: ShauttConstants.CLICK_EDIT_PRODUCT_TYPE_ROW_SHOPPING_LIST, properties: ["name" : item.nameEs, "quantity": "\(item.quantity ?? -91)", "diet": item.dietType ?? "unknown"])
            newView.setUpEditableViewWithItemDTO(presenter: self.shopListPresenterInteractor! as! ShopListPresenterInteractor, productType: item)
            self.view.addSubview(newView)
        }
    }
    
    
}

extension ShoppingListViewController: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.requestLocation()
            break
        case .denied, .restricted:
            break
        default:
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        
        storePostalCode(location: location)
        self.locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    
}

extension ShoppingListViewController: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound, .badge])
    }
}

extension ShoppingListViewController: MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        if let dataUser = KeychainSwift().getData("current_user") {
            do {
                let decoder = JSONDecoder()
                let user = try decoder.decode(UserLogin.self, from: dataUser)
                UserInitConfigurationManager().addMessagingTokenToUser(user: user, messagingToken: fcmToken)
            } catch {
                print(error)
                print(error.localizedDescription)
            }
        }
    }
}
