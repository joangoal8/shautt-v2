//
//  ProductCategoriesViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 09/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

class ProductCategoriesViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var itemPresenterInteractor: ProductItemPresenterInteractor?
    
    var itemsByCategories = [CategoryProductTypesDTO]()
    var filteredCategoryItems = [CategoryProductTypesDTO]()
    
    var isPreFiltered = false
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var customCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if itemPresenterInteractor == nil {
            self.itemPresenterInteractor = ProductItemPresenterInteractor(self)
        }
        
        self.itemPresenterInteractor?.retrieveCategoryItems()
        
    }
    
    @objc func handleExpandClose(button: UIButton) {
        
        var categoryItems = extractedCategoriesProductsRequired()
        if var isExpanded =  categoryItems[button.tag].isExpanded {
            isExpanded.toggle()
            categoryItems[button.tag].isExpanded = isExpanded
        } else {
            categoryItems[button.tag].isExpanded = true
        }
        
        self.fillCategoryItem(categoriesItems: categoryItems)
        customCollectionView.reloadData()
    }
    
    fileprivate func fillCategoryItem(categoriesItems: [CategoryProductTypesDTO]) {
        if (self.searchBar.text != nil && self.searchBar.text != "") {
            self.filteredCategoryItems = categoriesItems
        } else {
            self.itemsByCategories = categoriesItems
        }
    }
    
    @objc func showItemsDetails(button: CustomItemButton) {
        let addItemToShoppingBasketView = Bundle.main.loadNibNamed("AddItemToShoppingBasketView", owner: nil, options: nil)?[0] as? AddItemToShoppingBasketView
        if let newView = addItemToShoppingBasketView, let productType = button.productType {
            newView.setUpViewWithProductType(presenter: self.itemPresenterInteractor!, productType: productType)
            searchBar.resignFirstResponder()
            view.addSubview(newView)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "ItemCollectionHeader", for: indexPath) as! ItemsCategoryHeaderCollectionReusableView

        let sections = extractedCategoriesProductsRequired()
        
        header.headerButton.setTitle(" \(sections[indexPath.section].name)", for: .normal)
        header.setCustomTarget(self, action: #selector(handleExpandClose), for: .touchUpInside, section: indexPath.section)
        
        return header
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        let sections = extractedCategoriesProductsRequired()
        return sections.count
    }
    
    fileprivate func extractedCategoriesProductsRequired() -> [CategoryProductTypesDTO] {
        return (self.searchBar.text != nil && self.searchBar.text != "") ? self.filteredCategoryItems : self.itemsByCategories
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        guard
            !extractedCategoriesProductsRequired().isEmpty else {
                return 1
        }
        
        let sections = extractedCategoriesProductsRequired()
        if let isExpanded = sections[section].isExpanded, isExpanded {
            
            return sections[section].items.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollCell", for: indexPath) as! ProductCollectionViewCell
        
        let productType = extractedCategoriesProductsRequired()[indexPath.section].items[indexPath.row]
        
        //let image = loadImage(urlImage: URL(string: productType.principalImage))

        //collectionCell.imageProduct.image = image
        //collectionCell.imageProduct.contentMode = .scaleAspectFit
        collectionCell.nameTxt.text = productType.nameEs
        
        collectionCell.setCustomTarget(self, action: #selector(showItemsDetails), for: .touchUpInside, productType: productType)
        
        return collectionCell
    }
    
    fileprivate func extractedMapOfItemsRequired() -> [String : [ItemDTO]] {
        return [String : [ItemDTO]]()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ProductCategoriesViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        var newFilteredItems = [CategoryProductTypesDTO]()
        
        if (self.searchBar.text != nil && self.searchBar.text != "") {
            
            itemsByCategories.forEach { (categoryItem) in
                
                let filterPerProd = categoryItem.items.filter { (productType) -> Bool in
                    return productType.nameEs.uppercased().contains(searchBar.text!.uppercased())
                }
                
                if !filterPerProd.isEmpty {
                   newFilteredItems.append(CategoryProductTypesDTO(name: categoryItem.name, items: filterPerProd, isExpanded: false))
                }
            }
        }
        
        filteredCategoryItems = newFilteredItems
        
        self.customCollectionView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

extension ProductCategoriesViewController: ItemPresenterToViewProtocol {
    
    func defineCategoryProducts(categories: [CategoryProductTypesDTO]) {
        self.itemsByCategories = categories
        DispatchQueue.main.async {
            self.customCollectionView.reloadData()
        }
    }
    
}
