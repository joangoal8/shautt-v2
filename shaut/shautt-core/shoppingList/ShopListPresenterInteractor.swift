//
//  ShopListPresenterInteractor.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 01/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import KeychainSwift

class ShopListPresenterInteractor {
    
    weak var view: ShoppingListPresenterToViewProtocol?
    var items = [ProductType]()
    var images = [String:Data]()
    var shopListFirebaseDataProvider: ShopListFirebaseDataProvider?
    let updateItemService: UpdateItemSBService
    
    init(_ tableViewController: ShoppingListPresenterToViewProtocol) {
        self.view = tableViewController
        updateItemService = InjectionEngine.updateItemSBService
        updateItemService.updateItemToShoppingListDelegate = self
    }
    
    fileprivate func getShopListFirebaseDataProvider() -> ShopListFirebaseDataProvider {
        if let shopListFirebaseProvider = shopListFirebaseDataProvider {
            return shopListFirebaseProvider
        }
        shopListFirebaseDataProvider = ShopListFirebaseDataProvider(self)
        return shopListFirebaseDataProvider!
    }
    
}

extension ShopListPresenterInteractor: ShoppingListViewToPresenterInteractorProtocol {
    
    func retrieveShoppingList() {
        let shopListFBProvider = getShopListFirebaseDataProvider()
        shopListFBProvider.retrieveShoppingList()
    }
    
    func removeItemFromShoppingList(productType: ProductType) {
        if let shoppingBasketId = KeychainSwift().get(ShauttConstants.KEYCHAIN_SHOOPINGBASKETID_KEY) {
            updateItemService.removeItemToSB(productType: productType, shoppingBasketId: shoppingBasketId)
        }
    }
    
    func editItemShoppingList(productType: ProductType) {
        if let shoppingBasketId = KeychainSwift().get(ShauttConstants.KEYCHAIN_SHOOPINGBASKETID_KEY) {
           updateItemService.editItemToSB(productType: productType, shoppingBasketId: shoppingBasketId)
        }
    }
}

extension ShopListPresenterInteractor: ShoppingListEntityToPresenterInteractorProtocol {
        
    func addItemsAndPresentShoppingListTable(items: [ProductType]) {
        self.items = items
        view?.drawShoppingListTable(items: items)
    }
    
}

extension ShopListPresenterInteractor: UpdateItemToShoppingListDelegate {
    
    func updateShoppingBasket(items: [ProductType]) {
        self.items = items
        view?.drawShoppingListTable(items: items)
    }
}
