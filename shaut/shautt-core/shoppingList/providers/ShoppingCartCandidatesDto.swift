//
//  ShoppingCartCandidatesDto.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 04/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct ShoppingCartCandidatesResponse: Codable {
    let results: [ShoppingCartCandidateDto]
}

struct ShoppingCartCandidateDto: Codable {
    var shoppingCartProducts: [ShoppingCartProductTypeDto]
    var totalAmount: Decimal
    let stores: [StoreDto]
    let provider: String
}

struct StoreDto: Codable {
    
    let id: String
    let name: String
    let providerName: String
    let providerId: String
    let email: String
    let minimumOrder: Int
    let imageUrl: String
    var categoryProvided: String?
}

struct ShoppingCartProductTypeDto: Codable {
    
    var selectedProduct: ShoppingCartProductDto
    let otherOptionsProducts: [ShoppingCartProductDto]
    var quantity: Int
    let productType: ProductTypesInformation
}

struct ShoppingCartProductDto: Codable {
    
    let id: String
    let name: String
    let providerName: String
    let providerItemId: String
    let price: Decimal
    let imageUrl: String
    let storeId: String
    var productTypeProvided: String?
    var varietyProvided: String?
    var manufacturerName: String?
    let limitUnits: Int
    let productTypeCalculated: String
    let score: Double
}

