//
//  MapShopPresenterInteractor.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 08/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import GoogleMaps
import KeychainSwift

class LocationPresenterInteractor: ShoppingLocationViewToPresenterInteractorProtocol {
    
    var googlePlaceDataProvider: GooglePlaceDataProvider?
    
    var keyChainSwift: KeychainSwift?
    
    func fetchShoppingLocation(coordinate: CLLocationCoordinate2D) {
        let shopProviderData = self.getGooglePlaceDataProvider()
        
        shopProviderData.fetchPostalCodeLocation(coordinate: coordinate) { (addressComponents) in
            
            let postalCodeAddressComp = addressComponents.filter { (addressComp) -> Bool in
                return addressComp.types[0] == "postal_code"
            }.map { (addressComp) -> String in
                return addressComp.longName
            }.first
            
            if let postalCode = postalCodeAddressComp {
                self.getKeychainSwift().set(postalCode, forKey: "postal_code")
            }
        }
    }
    
    fileprivate func getGooglePlaceDataProvider() -> GooglePlaceDataProvider {
        if let googlePlaceProvider = googlePlaceDataProvider {
            return googlePlaceProvider
        }
        googlePlaceDataProvider = GooglePlaceDataProvider()
        return googlePlaceDataProvider!
    }
    
    fileprivate func getKeychainSwift() -> KeychainSwift {
        if let keyChain = keyChainSwift {
            return keyChain
        }
        keyChainSwift = KeychainSwift()
        return keyChainSwift!
    }
    
    func retrievePostalCode() -> String {
        var postalCode = ""
        if let postcode = KeychainSwift().get("postal_code") {
            postalCode = postcode
        }
        return postalCode
    }
    
    func storePostcodeInKeychain(postalcode: String) {
        self.getKeychainSwift().set(postalcode, forKey: "postal_code")
        print("upadating")
    }
}
