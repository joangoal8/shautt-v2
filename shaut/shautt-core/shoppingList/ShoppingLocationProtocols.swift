//
//  MapShowProtocols.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 08/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import GoogleMaps

protocol ShoppingLocationViewToPresenterInteractorProtocol: class {
    
    func fetchShoppingLocation(coordinate: CLLocationCoordinate2D)
    
    func retrievePostalCode() -> String
    
    func storePostcodeInKeychain(postalcode: String)
}
