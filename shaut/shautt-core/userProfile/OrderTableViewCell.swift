//
//  OrderTableViewCell.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 16/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

class OrderTableViewCell: UITableViewCell {

    @IBOutlet weak var orderContainer: UIView!
    
    @IBOutlet weak var addressIcon: UIImageView!
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var storeImage1: UIImageView!
    @IBOutlet weak var storeImage2: UIImageView!
    
    @IBOutlet weak var storeNameLabel: UILabel!
    
    @IBOutlet weak var clockIcon: UIImageView!
    @IBOutlet weak var deliveryTimeLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
