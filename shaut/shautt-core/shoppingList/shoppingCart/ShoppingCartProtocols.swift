//
//  ShoppingCartProtocols.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 07/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

protocol ShoppingCartViewToPresenterInteractorProtocol: class {
    
    func storeShoppingCart(shoppingCart: ShoppingCartCandidateDto)
    
    func createMasterCart(shoppingCart: ShoppingCartCandidateDto)
    
}

protocol ShoppingCartPresenterToViewProtocol: class {
    
    func updateShoppingCartView(shoppingCart: ShoppingCartCandidateDto)
    
    func provideMasterCart(masterCart: MasterCartDto)
    
    func displayError()
}

protocol ShoppingCartEntityToPresenterInteractorProtocol: class {
    
    func provideMasterCart(masterCart: MasterCartDto)
    
    func presentError()
}

protocol EditShoppingCartViewToPresenterInteractorProtocol: class {
    
    func updateShoppingCartProducts(shoppingCart: ShoppingCartCandidateDto)
    
}

protocol EditSingleShoppingCartViewToPresenterInteractorProtocol: class {
    
    func updateShoppingCartProduct(shoppingCartProduct: ShoppingCartProductTypeDto, shoppingCartProductIndex: Int)
    
}
