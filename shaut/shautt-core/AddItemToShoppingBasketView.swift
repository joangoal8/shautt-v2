//
//  AddItemToShoppingBasketView.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 15/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit
import Firebase
import Mixpanel

class AddItemToShoppingBasketView: UIView {
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var itemImage: UIImageView!
    
    @IBOutlet weak var itemNameLabel: UILabel!
    
    @IBOutlet weak var addToShoppingBktBtn: UIButton!
    @IBOutlet weak var amountLabel: UILabel!

    var itemPresenterInteractor: ProductItemPresenterInteractor?
    var shopListPresenterInteractor: ShopListPresenterInteractor?
    
    var productType: ProductType?
    var isEditable: Bool = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.backgroundColor = UIColor.gray.withAlphaComponent(0.4)
        self.frame = UIScreen.main.bounds
    }
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     */
    override func draw(_ rect: CGRect) {
        containerView.frame = UIScreen.main.bounds
        addToShoppingBktBtn.layer.masksToBounds = true
        addToShoppingBktBtn.layer.cornerRadius = 10
        animateIn()
    }
    
    @objc fileprivate func animateOut() {
        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.8, options: .curveEaseIn, animations: {
            self.containerView.transform = CGAffineTransform(translationX: 0, y: +self.frame.height)
        }) { (complete) in
            if complete {
                self.removeFromSuperview()
            }
        }
    }
    
    @objc fileprivate func animateIn() {
        self.containerView.transform = CGAffineTransform(translationX: 0, y: +self.frame.height)
        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.8, options: .curveEaseIn, animations: {
            self.containerView.transform = .identity
        })
    }
    
    func setUpViewWithProductType(presenter: ProductItemPresenterInteractor, productType: ProductType) {
        self.productType = productType
        self.itemPresenterInteractor = presenter
        commonSetup(productType)
    }
    
    fileprivate func commonSetup(_ productType: ProductType) {
        itemImage.load(path: "products/images/\(productType.itemId).png")
        itemNameLabel.text = productType.nameEs
        itemNameLabel.numberOfLines = 2
        if let quantity = productType.quantity {
            amountLabel.text = "\(quantity)"
        } else {
            if var product = self.productType {
                product.quantity = 1
                self.productType?.quantity = product.quantity
                amountLabel.text = "\(product.quantity!)"
            }
        }
    }
    
    func setUpEditableViewWithItemDTO(presenter: ShopListPresenterInteractor, productType: ProductType) {
        self.productType = productType
        self.shopListPresenterInteractor = presenter
        commonSetup(productType)
        self.isEditable = true
        self.addToShoppingBktBtn.setTitle("Editar", for: .normal)
    }
    
    @IBAction func removeItem(_ sender: Any) {
        if let product = productType {
            Mixpanel.mainInstance().track(event: ShauttConstants.DELETE_PRODUCT_TYPE_SHOPPING_LIST, properties: ["name" : product.nameEs, "quantity": "\(product.quantity ?? -91)", "diet": product.dietType ?? "unknown"])
            self.shopListPresenterInteractor?.removeItemFromShoppingList(productType: product)
        }
        
        animateOut()
    }
    
    @IBAction func closeView(_ sender: Any) {
        animateOut()
    }
    
    @IBAction func minusOne(_ sender: Any) {
        guard let item = productType else {
            return
        }
        if var quantity = item.quantity, quantity > 1 {
            quantity -= 1
            productType?.quantity = quantity
            amountLabel.text = "\(quantity)"
        }
    }
    
    @IBAction func plusOne(_ sender: Any) {
        guard var item = productType else {
            return
        }
        if var quantity = item.quantity {
            quantity += 1
            item.quantity = quantity
            productType = item
            amountLabel.text = "\(quantity)"
        }
    }
    
    @IBAction func addItemToShoppingBasket(_ sender: Any) {
        if isEditable {
            if let shopListPresentInt = shopListPresenterInteractor {
                Analytics.logEvent(ShauttConstants.EDIT_PRODUCT_TO_SHOPPING_LIST, parameters: ["view" : "editView"])
                if let product = productType {
                    Mixpanel.mainInstance().track(event: ShauttConstants.EDIT_PRODUCT_TYPE_SHOPPING_LIST, properties: ["name" : product.nameEs, "quantity": "\(product.quantity ?? -91)", "diet": product.dietType ?? "unknown"])
                }
                shopListPresentInt.editItemShoppingList(productType: productType!)
            }
        } else {
            Analytics.logEvent(ShauttConstants.ADD_PRODUCT_TO_SHOPPING_LIST, parameters: ["view" : "addView"])
            if let product = productType {
                Mixpanel.mainInstance().track(event: ShauttConstants.ADD_PRODUCT_TYPE_TO_SHOPPING_LIST, properties: ["name" : product.nameEs, "quantity": "\(product.quantity ?? -91)", "diet": product.dietType ?? "unknown"])
            }
        
            itemPresenterInteractor!.addItemToShoppingBasket(productType: productType!)
        }
        
        animateOut()
    }
    
    @IBAction func compareProductType(_ sender: Any) {
        Analytics.logEvent(ShauttConstants.COMPARE_PRODUCT_TO_SHOPPING_LIST, parameters: ["view" : "addEditView"])
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ComparatorView")
        if let comparatorViewController = viewController as? ComparatorViewController {
            let comparatorPresenter = ComparatorPresenterInteractor()
            comparatorViewController.comparatorPresenter = comparatorPresenter as ShoppingListComparatorViewToPresenter
            comparatorPresenter.comparatorView = comparatorViewController
        
            comparatorPresenter.compareProductType(productType: productType!)
            viewController.modalPresentationStyle = .fullScreen
            self.superview!.findViewController()!.present(comparatorViewController, animated: true, completion: nil)
        }
    }
    
}
