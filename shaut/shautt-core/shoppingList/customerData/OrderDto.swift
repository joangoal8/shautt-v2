//
//  OrderDto.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 10/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct OrderDtoResponse: Codable {
    
    let result: OrderDto
}

struct OrderDto: Codable {
    
    let uuid: String
    let providerOrderId: String?
    let amount: Decimal
    let storeRef: String
    let address: String
    let city: String
    let postalCode: String
    let phone: String
    let deliveryTime: DeliveryTimeDto
}

struct DeliveryTimeDto: Codable {
    
    let hour: Int
    let year: Int
    let month: String
    let dayOfMonth: Int
    let dayOfWeek: String
    let dayOfYear: Int
    let monthValue: Int
}
