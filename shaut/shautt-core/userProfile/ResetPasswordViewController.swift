//
//  ResetPasswordViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 13/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileUserName: UILabel!
    
    @IBOutlet weak var pwdContainterView: UIView!
    
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var newPasswordField: UITextField!
    @IBOutlet weak var repeatedNewPassordField: UITextField!
    
    var interactor: ResetPasswordViewToPresenterInteractorProtocol?
    var user: UserLogin?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        viewSetUp(view: pwdContainterView)
        passwordField.borderStyle = .none
        newPasswordField.borderStyle = .none
        repeatedNewPassordField.borderStyle = .none
        if let user = user {
            let emailWithoutHost = user.email.components(separatedBy: "@")
            profileUserName.text = "\(emailWithoutHost[0])"
            profileImage.layer.cornerRadius = profileImage.frame.height/2
            profileImage.load(path: "users/\(user.id).png")
        }
        
    }
    
    fileprivate func viewSetUp(view: UIView) {
        view.layer.cornerRadius = 21
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        view.layer.shadowOpacity = 1
    }
    
    @IBAction func backToWindow(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func changePassword(_ sender: Any) {
        if passwordField.text == "" || newPasswordField.text == "" || repeatedNewPassordField.text == "" {
            let message = "Porfavor, rellena los dos campos correctamente. Muchas gracias ☺️"
            displayRegisterAlert(message: message)
        } else if newPasswordField.text == repeatedNewPassordField.text {
            if let user = user {
                interactor?.resetUserPassword(email: user.email ,password: passwordField.text!, newPassword: newPasswordField.text!)
            }
        } else {
            let message = "Disculpa, las contraseñas no coinciden. Inténtelo de nuevo ☺️."
            displayRegisterAlert(message: message)
        }
    }
    
    fileprivate func displayRegisterAlert(message: String) {
        let alertController = UIAlertController(title: "Error al actualizar la contraseña", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    fileprivate func displayNotification(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ResetPasswordViewController: ResetPasswordPresenterToViewProtocol {
    
    func displayPasswordNotification() {
        DispatchQueue.main.async {
            self.displayNotification(title: "Actualizada", message: "Su contraseña ha sido actualizada correctamente 🥳")
        }
    }
    
    func displayPasswordError() {
        DispatchQueue.main.async {
            let message = "Ha habido un error actualizando su contraseña 😞. Inténtelo más tarde"
            self.displayRegisterAlert(message: message)
        }
    }
    
}

extension ResetPasswordViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
