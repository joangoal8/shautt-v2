//
//  OrdersViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 16/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

class OrdersViewController: UIViewController {

    @IBOutlet weak var orderTableView: UITableView!
    
    var interactor: OrderViewToPresenterInteractorProtocol?
    
    var user: UserLogin?
    var userOrdersDto = [UserOrderDto]()
    
    var shauttSpinner: ShauttSpinnerView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        orderTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        if let user = user {
            self.interactor?.retrieveOrders(userId: user.id)
            activateSpinner()
        }
        
    }
    
    private func activateSpinner() {
        shauttSpinner = ShauttSpinnerView()
        
        view.addSubview(shauttSpinner!)
        
        shauttSpinner?.center = self.view.center
         
        shauttSpinner!.animate(shauttSpinner!.circle1, counter: 1)
        shauttSpinner!.animate(shauttSpinner!.circle2, counter: 3)
        
        view.isUserInteractionEnabled = false
    }

    @IBAction func backToCustomerData(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    fileprivate func stopActivityIndicator() {
        self.shauttSpinner?.removeFromSuperview()
    }
}

extension OrdersViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if userOrdersDto.isEmpty {
            return 1
        }
        return userOrdersDto.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let orderCell = tableView.dequeueReusableCell(withIdentifier: "OrderCell", for: indexPath) as! OrderTableViewCell
        
        if userOrdersDto.isEmpty {
            return orderCell
        }
        
        viewSetUp(view: orderCell.orderContainer)
        
        orderCell.addressIcon.image = UIImage(named: "addressIcon")
        orderCell.clockIcon.image = UIImage(named: "clockIcon")
        
        let userOrder = userOrdersDto[indexPath.row]
        let order = userOrder.order
        orderCell.addressLabel.text = order.address
        let stores = order.storeRef.components(separatedBy: "+")
        if stores.count < 2 {
            orderCell.storeImage1.load(path: "stores/\(stores[0]).png")
            orderCell.storeNameLabel.text = "\(stores[0])"
        } else {
            orderCell.storeImage1.load(path: "stores/\(stores[0]).png")
            orderCell.storeImage2.load(path: "stores/\(stores[1]).png")
            orderCell.storeNameLabel.text = order.storeRef
        }
        orderCell.priceLabel.text = "\(order.amount.round()) €"
        orderCell.deliveryTimeLabel.text = "\(order.deliveryTime.dayOfMonth)/\(order.deliveryTime.monthValue)/\(order.deliveryTime.year) \(order.deliveryTime.hour):00 h"
        
        return orderCell
    }
    
    fileprivate func viewSetUp(view: UIView) {
        view.layer.cornerRadius = 21
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        view.layer.shadowOpacity = 1
    }
}

extension OrdersViewController: OrderPresenterToViewProtocol {
    
    func displayOrders(orders: [UserOrderDto]) {
        DispatchQueue.main.async {
            self.userOrdersDto = orders
            self.view.isUserInteractionEnabled = true
            self.stopActivityIndicator()
            self.orderTableView.reloadData()
        }
    }
    
}
