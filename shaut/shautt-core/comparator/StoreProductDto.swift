//
//  StoreProductDto.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 12/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct StoreProductDtoResponse: Codable {
    
    let result: StoreProductTypesDto
}

struct StoreProductTypesDto: Codable {
    
    let products: [StoreProductDto]
    let productType: ProductType
}

struct StoreProductDto: Codable {
    
    let product: ShoppingCartProductDto
    let store: StoreDto
}

