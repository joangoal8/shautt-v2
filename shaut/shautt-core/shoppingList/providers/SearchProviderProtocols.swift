//
//  ProviderProtocols.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 01/09/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

protocol SearchProviderViewToPresenterInteractorProtocol: class {
    
    func searchShoppingCarts(postcode: String, shopList: [ProductType])
}

protocol ProviderPresenterToViewProtocol: class {
    
    func drawShoppingCarts(shoppingCarts: [ShoppingCartCandidateDto])
    
    func alertError()
    
}

protocol ProviderEntityToPresenterInteractorProtocol: class {
    
    func provideShoppingCarts(shoppingCarts: [ShoppingCartCandidateDto])
    
    func presentError()
}
