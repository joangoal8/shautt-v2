//
//  ShauttConfigResponse.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 26/12/20.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct ShauttConfigResponse: Codable {
    
    let host: String
}
