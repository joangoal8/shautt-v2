//
//  PurchaseRequest.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 10/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct PurchaseRequest: Codable {
    
    let masterCartId: String
    let cartIds: [String]
    let providerProducts: [MasterCartItemDto]
    let cartDeliveryTime: CartDeliveryTime
    let providerPayment: PaymentDataDto
    //let isTest = "true"
    var userId: String?
    let providerName: String
}

struct CartDeliveryTime: Codable {
    
    let deliveryTime: String
}


