//
//  IntentsManager.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 11/09/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import Intents

class IntentsManager {
    
    static let sharedInstance = IntentsManager()
    
    let sharedDefaults = UserDefaults(suiteName: ShauttConstants.GROUP_ID)
    
    private init() {}
    
    func add(task: String, completionHandler: @escaping (String?, Error?) -> Void) {
        print(task)
        //allow add more than one in one sentence also in the BE
        let itemToAddName = task.replacingOccurrences(of: "[", with: "").replacingOccurrences(of: "]", with: "")
        let itemsToAdd = itemToAddName.components(separatedBy: " y ")
        if let shoppingBasketId = sharedDefaults?.string(forKey: ShauttConstants.USER_DEFAULTS_SHOOPINGBASKETID_KEY) {
            print( "The info is \(itemToAddName) and shoppingBasketId \(shoppingBasketId)")
            if itemsToAdd.count < 2 {
                SiriIntentService.sharedInstance.addItemByNameToSB(itemName: itemToAddName, shoppingBasketId: shoppingBasketId) { (itemName, error) in
                    if let err = error {
                        print(err.localizedDescription)
                        completionHandler(nil, err)
                        return
                    }
                    print( "The itemName is \(itemName!)")
                    completionHandler(itemName, nil)
                }
            } else {
                for item in itemsToAdd {
                    SiriIntentService.sharedInstance.addMultiItemsByNameToSB(itemName: item, shoppingBasketId: shoppingBasketId)
                }
                completionHandler(itemToAddName, nil)
            }
        } else {
            completionHandler(nil, nil)
        }
        
    }
    
    
}
