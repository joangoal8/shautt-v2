//
//  UserOrderDto.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 16/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct UserOrderDtoResponse: Codable {
    
    let results: [UserOrderDto]
}

struct UserOrderDto: Codable {
    
    let order: OrderDto
    let orderContainerProducts: [OrderContainerProduct]
}

struct OrderContainerProduct: Codable {
    
    let uuid: String
    let orderId: String
    let product: MasterCartItemDto
    
}
