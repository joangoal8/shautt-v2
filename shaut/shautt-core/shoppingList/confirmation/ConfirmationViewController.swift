//
//  ConfirmationViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 09/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

class ConfirmationViewController: UIViewController {

    @IBOutlet weak var confirmationContainer: UIView!
    @IBOutlet weak var orderInfoContainer: UIView!
    @IBOutlet weak var addressOrder: UILabel!
    @IBOutlet weak var storeImage: UIImageView!
    @IBOutlet weak var storeImage2: UIImageView!
    
    @IBOutlet weak var storeOrder: UILabel!
    @IBOutlet weak var timeOrder: UILabel!
    @IBOutlet weak var priceOrder: UILabel!
    
    var shoppingCartDto: ShoppingCartCandidateDto?
    var orderDto: OrderDto?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        viewSetUp(view: confirmationContainer)
        viewSetUp(view: orderInfoContainer)
        
        if let order = orderDto, let shoppingCart = shoppingCartDto {
            addressOrder.text = order.address
            if shoppingCart.stores.count < 2 {
                storeImage.load(path: "stores/\(shoppingCart.stores[0].name).png")
                storeOrder.text = "\(shoppingCart.stores[0].name)"
            } else {
                storeImage.load(path: "stores/\(shoppingCart.stores[0].name).png")
                storeImage2.load(path: "stores/\(shoppingCart.stores[1].name).png")
                storeOrder.text = "\(shoppingCart.stores[0].name) + \(shoppingCart.stores[1].name)"
            }
            timeOrder.text = "\(order.deliveryTime.dayOfMonth)/\(order.deliveryTime.monthValue)/\(order.deliveryTime.year) \(order.deliveryTime.hour):00 h"
            priceOrder.text = "\(order.amount.round()) €"
        }
    }
    
    fileprivate func viewSetUp(view: UIView) {
        view.layer.cornerRadius = 21
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        view.layer.shadowOpacity = 1
    }

    
    @IBAction func closeShoppingListFlow(_ sender: Any) {
        self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
