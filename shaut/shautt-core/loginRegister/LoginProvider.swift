//
//  LoginProvider.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 08/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

enum LoginProvider: String, Codable {
    case PasswordEmail, Google, Facebook, Apple
}
