//
//  MasterCartItemDto.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 08/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct MasterCartItemDto: Codable {
    
    let itemId: String
    let productType: String
    let providerItemId: String
    let providerName: String
    let quantity: Int
    
}
