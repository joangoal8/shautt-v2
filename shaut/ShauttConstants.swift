//
//  ShautConstants.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 13/09/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct ShauttConstants {
    
    //Firebase metrics
    static let GROUP_ID = "group.com.jgoal.ios.shaut"
    static let SHAUTT_IOS_TOPIC = "shautt_ios_users"
    static let KEYCHAIN_SHOOPINGBASKETID_KEY = "user_shoppingBasketId"
    static let USER_DEFAULTS_SHOOPINGBASKETID_KEY = "user_shoppingBasketId"
    static let SIRI_NOTIFICATION_NAME = "addProduct"
    static let SIRI_ADD_PRODUCT_NAME_KEY = "siri_addproduct_name"
    
    static let LIST_SHOPPING_LIST_PRODUCTS = "list_shopping_list_products"
    static let ADD_PRODUCT_TO_SHOPPING_LIST = "add_product_shopping_list_manual"
    static let EDIT_PRODUCT_TO_SHOPPING_LIST = "edit_product_shopping_list"
    static let CLICK_COMPARE_SHOPPING_LIST = "click_compare_shopping_list"
    static let COMPARE_PRODUCT_TO_SHOPPING_LIST = "compare_product_shopping_list"
    static let SCAN_BARCODE_PRODUCT = "scan_barcode_product"
    static let SHOW_EDIT_POSTCODE = "edit_postcode"
    static let COMPARE_SHOPPING_LIST = "compare_shopping_list"
    static let CHOSE_STORE_IN_SHOPPING_LIST = "chose_store_in_shopping_list"
    static let EDIT_SINGLE_SHOPPING_CART_PRODUCT = "edit_single_shopping_cart_product"
    static let EDIT_ALL_SHOPPING_CART_PRODUCT = "edit_all_shopping_cart_product"
    static let CONFIRM_SHOPPING_CART = "confirm_shopping_cart"
    static let INI_ADD_ADDRESS_TO_SHOPPING_CART = "ini_add_address_to_shopping_cart"
    static let ADD_ADDRESS_TO_SHOPPING_CART = "add_address_to_shopping_cart"
    static let CHECK_SHOPPING_CART_DELIVERY_TIME = "check_shopping_cart_delivery_time"
    static let GET_SHOPPING_CART_DAY_TIME = "get_shopping_cart_day_time"
    static let ADD_SHOPPING_CART_DELIVERY_TIME = "add_shopping_cart_delivery_time"
    static let PURCHASE_SHOPPING_CART = "purchase_shopping_cart"
    static let ALLOW_PUSH_NOTIFICATIONS = "allow_push_notifications"
    static let DOES_NOT_ALLOW_PUSH_NOTIFICATIONS = "does_not_allow_push_notifications"
    static let ERROR_PUSH_NOTIFICATIONS = "allow_push_notifications"
    
    //Mixpanel metrics
    static let CLICK_EMAIL_PWD_LOGIN = "User login with email and pwd"
    static let CLICK_SOCIAL_GOOGLE_LOGIN = "User login with google"
    static let CLICK_SOCIAL_APPLE_LOGIN = "User login with apple"
    static let ERROR_USER_LOGIN = "Error trying to login"
    static let SHOW_REGISTER_VIEW = "Switch to register view from login"
    static let CLICK_REGISTER_BUTTON = "Click to register"
    static let ERROR_USER_REGISTER = "Error trying to register"
    static let CLICK_SHARE_SHOPPING_LIST = "Share shopping list with contacts"
    static let SYNC_SHOPPING_LIST = "Sync shopping list"
    static let CHECK_ALL_CATEGORY_PRODUCTS = "Show all category product types"
    static let CLICK_EDIT_PRODUCT_TYPE_ROW_SHOPPING_LIST = "Click shopping list row to edit a product type"
    static let ADD_PRODUCT_TYPE_TO_SHOPPING_LIST = "Add a new product type to the shopping list"
    static let EDIT_PRODUCT_TYPE_SHOPPING_LIST = "Edit a product type from shopping list"
    static let DELETE_PRODUCT_TYPE_SHOPPING_LIST = "Remove a product type from shopping list"
    static let CLICK_COMPARE_BUTTON_SHOPPING_LIST = "Click compare search button shopping list"
    static let ACCEPT_RECOGNIZED_POSTCODE = "Accept the postcode recognized by location"
    static let VIEW_SET_CUSTOM_POSTCODE = "View set the postcode for comparing"
    static let COMPARE_SHOPPING_LIST_BE = "Compare search shopping list with BE"
    static let CHOOSE_STORE_OPTION = "Choose store option"
    static let EDIT_SHOPPING_CART = "Edit the entire shopping cart"
    static let CONFIRM_UPDATE_ENTIRE_SHOPPING_CART = "Confirm an update of the entire shopping cart"
    static let STOP_EDITING_ENTIRE_SHOPPING_CART = "Close edition to the entire shopping cart"
    static let CLICK_EDIT_SINGLE_SHOPPING_CART_PRODUCT = "Edit a single shopping cart product"
    static let CLICK_INCREMENT_SINGLE_SHOPPING_CART_PRODUCT = "Click for adding quantity"
    static let CLICK_DECREASE_QUANTITY_SINGLE_SHOPPING_CART_PRODUCT = "Click reducing quantity"
    static let CONFIRM_UPDATE_SINGLE_SHOPPING_CART_PRODUCT = "Confirm an update a single shopping cart product"
    static let CLOSE_POPOUP_UPDATE_SINGLE_SHOPPING_CART_PRODUCT = "Close single shopping cart product popup edition"
    static let ACCEPT_AND_CONFIRM_SHOPPING_CART = "Confirm shopping cart"
    static let CLICK_TO_ADD_ADDRESS = "Click to add address in address view"
    static let ADD_ADDRESS_BY_GOOGLE = "Add address provided by google location"
    static let CONFIRM_ADDRESS_SHOPPING_CART = "Confirm shopping cart address information"
    static let CLOSE_ADDING_ADDRESS = "Close add address to the shopping cart"
    static let ERROR_FIELDS_ADDRESS_SHOPPING_CART = "Error in the shopping card address fields"
    static let ERROR_ADDING_SHOPPING_CART = "Error adding the address to shopping cart"
    static let CLICK_TO_ADD_DELIVERY_TIME = "Click to add delivery time in delivery time view"
    static let ERROR_CLICKING_ADD_DELIVERY_TIME = "Error clickinkg delivery time before address"
    static let CLICK_DELIVER_DAY_OPTION = "Click to check delivery day options"
    static let CONFIRM_DAY_HOUR_DELIVERY_TIME = "Confirm delivery time day hour option"
    static let PAY_AND_CONFIRM_SHOPPING_CART = "Click Pay and confirm order"
    static let SHOPPING_IS_CONFIRMED_SHOPPING_CART = "Shopping is confirmed"
    static let ACCEPT_PUSH_NOTIFICATIONS = "Allow push notification"
    static let DOES_NOT_ACCEPT_PUSH_NOTIFICATIONS = "Notifications are not allowed"
    static let ERROR_ALLOWING_PUSH_NOTIFICATIONS = "Error allowing push notifications"
    
    static let ADDING_PRODUCT_THROUGH_SIRI = "Adding a new product through Siri functionality"
}
