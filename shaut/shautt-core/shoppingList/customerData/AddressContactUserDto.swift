//
//  AddressContactUserDto.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 09/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct AddressContactUser {
    
    let addressUser: AddressUser
    let addressMore: String
    let phone: String
    
}

struct AddressUser {
    
    let address: String
    let city: String
    let postalCode: String
    
}
