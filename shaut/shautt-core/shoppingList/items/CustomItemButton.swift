//
//  CustomItemButton.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 15/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

class CustomItemButton: UIButton {

    var productType: ProductType?

    func loadImage(url: URL) -> UIImage {
        if let data = try? Data(contentsOf: url) {
            print(data)
            if let image = UIImage(data: data) {
                print("entroooo per returning")
                print(image)
                return image
            }
        }
        return UIImage(named: "noImage")!
    }
}
