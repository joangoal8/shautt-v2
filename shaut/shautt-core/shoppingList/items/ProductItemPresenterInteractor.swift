//
//  ProductItemPresenterInteractor.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 12/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import KeychainSwift

class ProductItemPresenterInteractor: ItemViewToPresenterInteractorProtocol {
    
    weak var view: ItemPresenterToViewProtocol?
    weak var addItemView: AddItemPresenterToViewProtocol?
    
    var itemsCategoryData = [CategoryProductTypesDTO]()
    var productCategoryDataProvider: ProductCategoryDataProvider?
    let updateItemService: UpdateItemSBService
    
    init(_ collectionViewController: ItemPresenterToViewProtocol) {
        self.view = collectionViewController
        updateItemService = InjectionEngine.updateItemSBService
        updateItemService.updateItemProductItemDelegate = self
    }
    
    func retrieveCategoryItems() {
        self.getProductFireStoreDataProvider().retrieveCategoriesItems()
    }

    fileprivate func getProductFireStoreDataProvider() -> ProductCategoryDataProvider {
        if let productItemFirestore = productCategoryDataProvider {
            return productItemFirestore
        }
        productCategoryDataProvider = ProductCategoryDataProvider(self)
        return productCategoryDataProvider!
    }
    
    func updateCategoryItems(categoryItems: [CategoryProductTypesDTO]) {
        self.itemsCategoryData = categoryItems
    }
}

extension ProductItemPresenterInteractor: ItemEntityToPresenterInteractorProtocol {
    
    func presentCategoryItems(categoriesItems: [CategoryProductTypesDTO]) {
        self.itemsCategoryData = categoriesItems
        self.view?.defineCategoryProducts(categories: self.itemsCategoryData)
    }

}

extension ProductItemPresenterInteractor: AddItemViewToPresenterInteractorProtocol {
    
    func addItemToShoppingBasket(productType: ProductType) {
        if let quantity = productType.quantity, quantity > 0, let shoppingBasketId = KeychainSwift().get(ShauttConstants.KEYCHAIN_SHOOPINGBASKETID_KEY) {
            let addItemService = InjectionEngine.updateItemSBService
            addItemService.updateItemProductItemDelegate = self
            addItemService.addItemToSB(productType: productType, shoppingBasketId: shoppingBasketId)
        }
    }
}

extension ProductItemPresenterInteractor: UpdateItemProductItemDelegate {
    
    func updateItemToShoppingBasket(productType: ProductType) {
        var categoriesProducts = [CategoryProductTypesDTO]()
        for var category in itemsCategoryData {
            if category.items.contains(productType), let index =
                category.items.firstIndex(of: productType) {
                category.items[index].quantity = productType.quantity
            }
            categoriesProducts.append(category)
        }
        self.itemsCategoryData = categoriesProducts
        view?.defineCategoryProducts(categories: self.itemsCategoryData)
    }
    
}
