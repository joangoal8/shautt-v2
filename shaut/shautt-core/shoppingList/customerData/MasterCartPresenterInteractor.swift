//
//  MasterCartPresenterInteractor.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 08/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import KeychainSwift
import GooglePlaces

class MasterCartPresenterInteractor {
    
    weak var view: MasterCartDataPresenterToViewProtocol?
    weak var addressView: AddressDataPresenterToViewProtocol?
    weak var timeView: TimeDeliveryDataPresenterToViewProtocol?
    
    var addressUser: AddressUser?
    
    var masterCartEngine: MasterCartDataProviderEngine?
    
    var keyChainSwift: KeychainSwift?
    
    var masterCartDto: MasterCartDto?
    var schedule = [ScheduleDeliveryTimeDto]()
    
    var token = ""
    
    init(_ viewController: MasterCartDataPresenterToViewProtocol) {
        self.view = viewController
    }
    
    fileprivate func getMasterCartEngine() -> MasterCartDataProviderEngine {
        if let masterCartEngineDataProvider = masterCartEngine {
            return masterCartEngineDataProvider
        }
        masterCartEngine = MasterCartDataProviderEngine(self)
        return masterCartEngine!
    }
    
    fileprivate func getKeychainSwift() -> KeychainSwift {
        if let keyChain = keyChainSwift {
            return keyChain
        }
        keyChainSwift = KeychainSwift()
        return keyChainSwift!
    }
    
    fileprivate func getUserId() -> String? {
        if let dataUser = KeychainSwift().getData("current_user") {
            do {
                let decoder = JSONDecoder()
                let user = try decoder.decode(UserLogin.self, from: dataUser)
                return user.id
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}

extension MasterCartPresenterInteractor: MasterCartDataViewToPresenterInteractorProtocol {
    
    func setMasterCart(masterCart: MasterCartDto) {
        self.masterCartDto = masterCart
    }
    
    func getDayTimeDelivery(cartIds: [String], providerName: String) {
        getMasterCartEngine().retrieveDeliveryTime(token: token, cartIds: cartIds, providerName: providerName)
    }
    
    func confirmMasterCart(masterCart: MasterCartDto, selectedTime: String, paymentMethod: PaymentDataDto) {
        masterCartEngine?.confirmPayment(token: token, masterCart: masterCart, selectedTime: selectedTime, paymentMethod: paymentMethod, userId: getUserId())
    }
    
}

extension MasterCartPresenterInteractor: AddressDataViewToPresenterInteractorProtocol {
    
    func processPlace(place: GMSPlace) {
        if let components = place.addressComponents {
            var route: String?
            var locality: String?
            var postcode: String?
            components.forEach { (addressComponent) in
                addressComponent.types.forEach { (type) in
                    switch type {
                    case "route":
                        route = addressComponent.name
                        break
                    case "locality":
                        locality = addressComponent.name
                        break
                    case "postal_code":
                        postcode = addressComponent.name
                    default:
                        break
                    }
                }
            }
            if let address = route, let city = locality {
                if let postalCode = postcode {
                    self.addressUser = AddressUser(address: address, city: city, postalCode: postalCode)
                } else {
                    if let postcode = getKeychainSwift().get("postal_code") {
                        self.addressUser = AddressUser(address: address, city: city, postalCode: postcode)
                    }
                }
                self.addressView?.updateAddressStreet(addressUser: self.addressUser!)
            }
        }
    }
    
    func storeAddresDetailsAndPhone(house: String, door: String, phone: String) {
        self.getMasterCartEngine().registerMasterCart(masterCart: masterCartDto!, address: AddressContactUser(addressUser: addressUser!, addressMore: "\(house) \(door)", phone: phone), token: token)
        addressView?.addressUpdateDismiss()
        view?.displaySpinnerLoading()
    }
}

extension MasterCartPresenterInteractor: DeliveryTimeDataViewToPresenterInteractorProtocol {
    
    func retrieveTimeDelivery() {
        if schedule.isEmpty {
            return
        }
        self.updateTimeDelivery(schedule: self.schedule)
    }
    
    func storeSelectedTimeDelivery(scheduleDay: ScheduleDayDto) {
        self.view?.updateDeliveryTime(scheduleDay: scheduleDay)
    }
    
}

extension MasterCartPresenterInteractor: MasterCartDataEntityToPresenterInteractorProtocol {
    
    func provideMasterCart(masterCart: MasterCartDto) {
        if let providedToken = masterCart.token {
            self.token = providedToken
        }
        
        view?.updateMasterCart(mastercart: masterCart)
    }
    
    func presentAddressError() {
        view?.alertAddressError()
    }
    
    func updateTimeDelivery(schedule: [ScheduleDeliveryTimeDto]) {
        self.schedule = schedule
        self.timeView?.updateTimeDelivery(schedule: schedule)
    }
    
    func presentDeliveryTimeError() {
        print("error with delivery")
        self.timeView?.displayTimeDeliveryError()
    }
    
    func provideOrderDto(order: OrderDto) {
        self.view?.updateOrder(order: order)
    }
    
    func presentConfirmationError() {
        self.view?.alertConfirmationError()
    }
}
