//
//  StoreProductCollectionViewCell.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 13/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

class StoreProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    
    @IBOutlet weak var storeImage: UIImageView!
   
    @IBOutlet weak var productPrice: UILabel!
    
}
