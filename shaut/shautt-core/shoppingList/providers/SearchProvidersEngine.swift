//
//  ProvidersEngine.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 01/09/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation
import Firebase

class SearchProvidersEngine {
    
    weak var interactor: ProviderEntityToPresenterInteractorProtocol?
    
    init(_ searchInteractor: SearchProviderPresenterInteractor) {
        self.interactor = searchInteractor
    }
    
    func searchShoppingCarts(postcode: String, itemTypes: [ProductTypesInformation]) {
        let urlString = "\(ShauttConfigService.shared.getShauttBEHostUrl())/\(postcode)/shoppingCarts"
        
        guard let url = URL(string: urlString) else {
            return
        }
        
        var params: String = ""
        
        let encoder = JSONEncoder()
        let searchShoppingCartsRequest = SearchShoppingCartsRequest(productTypes: itemTypes)
        
        if let jsonData = try? encoder.encode(searchShoppingCartsRequest) {
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                params = jsonString
            }
        }
        
        let postData = params.data(using: .utf8)
        
        var request = URLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(shauttKey, forHTTPHeaderField: "key")
        
        request.httpMethod = "POST"
        request.httpBody = postData
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let result = data {
                do {
                    let searchShoppingCartsResponse = try JSONDecoder().decode(ShoppingCartCandidatesResponse.self, from: result)
                    self.interactor?.provideShoppingCarts(shoppingCarts: searchShoppingCartsResponse.results)
                } catch {
                    print(error.localizedDescription)
                    self.interactor?.presentError()
                }
            } else {
                print(String(describing: error))
                self.interactor?.presentError()
            }
        }
        task.resume()
    }
}
