//
//  MasterCartDto.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 08/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct MasterCartDtoResponse: Codable {
    
    let result: MasterCartDto
}

struct MasterCartDto: Codable {
    
    let id: String
    let carts: [CartDto]
    let providerProducts: [MasterCartItemDto]
    let amount: Decimal
    var deliveryAddress: MasterCartDeliveryAddressDto?
    let quantity: Int
    let providerName: String
    var token: String?
}

struct CartDto: Codable {
    
    let id: String
    let quantity: Int
    let nextAvailableDeliveryTime: String
    let providerStoreDto: ProviderStoreDto
    var deliveryTime: String?
}

struct ProviderStoreDto: Codable {
    let name: String
}

struct MasterCartDeliveryAddressDto: Codable {
    
    let id: String
    let address: String
    let city: String
    let postalCode: String
    let phone: String
}

