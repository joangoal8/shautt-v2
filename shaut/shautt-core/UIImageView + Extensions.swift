//
//  UIImageView + Extensions.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 26/09/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit
import Firebase

extension UIImageView {
    
    func load(path: String) {
        DispatchQueue.global().async { [weak self] in
            let imageRef = Storage.storage().reference().child(path)
            imageRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
                
                if let errorResult = error {
                    print(errorResult.localizedDescription)
                }
                if let data = data, let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
    
    func load(url: URL) {
         DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}

extension UIImage {
    
    func resizeSizeCustomSize(width: Int = 128, height: Int = 128) -> UIImage {
        let targetSize = CGSize(width: width, height: height)
        // Determine the scale factor that preserves aspect ratio
        let widthRatio = targetSize.width / size.width
        let heightRatio = targetSize.height / size.height
        
        let scaleFactor = min(widthRatio, heightRatio)
        
        // Compute the new image size that preserves aspect ratio
        let scaledImageSize = CGSize(
            width: size.width * scaleFactor,
            height: size.height * scaleFactor
        )

        // Draw and return the resized UIImage
        let renderer = UIGraphicsImageRenderer(
            size: scaledImageSize
        )

        let scaledImage = renderer.image { _ in
            self.draw(in: CGRect(
                origin: .zero,
                size: scaledImageSize
            ))
        }
        
        return scaledImage
    }
}
