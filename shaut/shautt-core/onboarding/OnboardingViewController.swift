//
//  OnboardingViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 21/12/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

class OnboardingViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    private let onboardingSlides = OnboardingSlide.onboardingSlideCollection
    
    var collectionViewSize: CGSize?
    var onboardingPresenterInteractor : BasicOnboardingViewToPresenterInteractorProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if onboardingPresenterInteractor == nil {
            let presenterInteractor = OnboardingPresenterInteractor()
            presenterInteractor.basicOnboardingView = self
            onboardingPresenterInteractor = presenterInteractor
        }
        // Do any additional setup after loading the view.
        setupCollectionView()
    }
    
    private func setupCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        collectionView.collectionViewLayout = layout
        collectionView.contentInsetAdjustmentBehavior = .never
        collectionView.isPagingEnabled = true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension OnboardingViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return onboardingSlides.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "onboardingCell", for: indexPath) as! OnboardingCollectionViewCell
        let slide = onboardingSlides[indexPath.row]
        cell.configure(slide: slide)
        cell.actionButtonDidTapped = {
            self.handleStepOperation(at: indexPath)
        }
        return cell
    }
    
    private func handleStepOperation(at indexPath: IndexPath) {
        if indexPath.row == onboardingSlides.count - 1 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let mainTabBarController = storyboard.instantiateViewController(identifier: "MenuTabBarController")
            
            // This is to get the SceneDelegate object from your view controller
            // then call the change root view controller function to change to main tab bar
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
        } else if indexPath.row == onboardingSlides.count - 2 {
            onboardingPresenterInteractor?.isReferralOnboarding()
        } else {
            let nextItem = indexPath.item + 1
            let nextIndexPath = IndexPath(item: nextItem, section: 0)
            collectionView.scrollToItem(at: nextIndexPath, at: .top, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        guard let size = collectionViewSize else {
            let itemWidth = collectionView.bounds.width
            let itemHeight = collectionView.bounds.height
            collectionViewSize = CGSize(width: itemWidth, height: itemHeight)
            return collectionViewSize!
        }
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension OnboardingViewController : BasicOnboardingPresenterToViewProtocol {
    
    func showNextOnboardingStep(isReferralOnboarding: Bool) {
        DispatchQueue.main.async {
            if isReferralOnboarding, let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ReferralOnboardingController") {
                viewController.modalPresentationStyle = .fullScreen
                if let referralViewController = viewController as? ReferralOnboardingViewController {
                    let presenterInteractor = self.onboardingPresenterInteractor as? OnboardingPresenterInteractor
                    referralViewController.configurePresenterInteractor(presenterInteractor: presenterInteractor!)
                    self.present(referralViewController, animated: true, completion: nil)
                    return
                }
            }
            let nextIndexPath = IndexPath(item: self.onboardingSlides.count - 1, section: 0)
            self.collectionView.scrollToItem(at: nextIndexPath, at: .top, animated: true)
        }
    }
    
}
