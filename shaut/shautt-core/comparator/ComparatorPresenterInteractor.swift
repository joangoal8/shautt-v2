//
//  ComparatorPresenterInteractor.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 28/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

class ComparatorPresenterInteractor {
    
    weak var comparatorView: ComparatorPresenterToViewProtocol?
    var comparatorEngine: ComparatorEngine?
    
    fileprivate func getComparatorEngine() -> ComparatorEngine {
        if let comparatorDataProvider = comparatorEngine {
            return comparatorDataProvider
        }
        comparatorEngine = ComparatorEngine(self)
        return comparatorEngine!
    }
    
}


extension ComparatorPresenterInteractor: ShoppingListComparatorViewToPresenter {
    
    func compareProductType(productType: ProductType) {
        getComparatorEngine().compareProductType(productType: productType)
    }
    
}

extension ComparatorPresenterInteractor: ComparatorEntityToInteractor {
    
    func provideStoreProductsDto(storeProductType: StoreProductTypesDto) {
        self.comparatorView?.showStoreProducts(storeProductType: storeProductType)
    }
    
    func presentError() {
        self.comparatorView?.drawAlert()
    }
    
}
