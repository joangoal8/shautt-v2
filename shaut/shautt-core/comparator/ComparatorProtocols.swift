//
//  ComparatorProtocols.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 28/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

protocol ShoppingListComparatorViewToPresenter: class {
    
    func compareProductType(productType: ProductType)
}

protocol ComparatorEntityToInteractor: class {
    
    func provideStoreProductsDto(storeProductType : StoreProductTypesDto)
    
    func presentError()
}
