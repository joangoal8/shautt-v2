//
//  ScheduleDeliveryTimeDto.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 10/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct ScheduleDeliveryTimeDtoResponse: Codable {
    
    let schedules: [ScheduleDeliveryTimeDto]
}

struct ScheduleDeliveryTimeDto: Codable {
    
    let dayOfWeek: String
    let day: String
    let deliveryHours: [ScheduleDayDto]
}

struct ScheduleDayDto: Codable {
    let time: String?
    let amount: Decimal?
}
