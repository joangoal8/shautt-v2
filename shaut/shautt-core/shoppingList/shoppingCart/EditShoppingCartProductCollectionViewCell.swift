//
//  EditShoppingCartProductCollectionViewCell.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 06/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

class EditShoppingCartProductCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var shoppingCartProductImage: UIImageView!
    @IBOutlet weak var shoppingCartProductName: UILabel!
    @IBOutlet weak var shoppingCartProductPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
