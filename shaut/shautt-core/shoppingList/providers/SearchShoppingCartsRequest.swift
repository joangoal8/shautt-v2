//
//  DeliveryProviderShoppingBasketValueRequest.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 02/09/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import Foundation

struct SearchShoppingCartsRequest: Encodable {

    let productTypes: [ProductTypesInformation]
    
    init(productTypes: [ProductTypesInformation]) {
        self.productTypes = productTypes
    }
}
