//
//  OnboardingCollectionViewCell.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 21/12/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit
import Lottie

class OnboardingCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var stepTitle: UILabel!
    @IBOutlet weak var animationView: AnimationView!
    @IBOutlet weak var stepExplanation: UILabel!
    @IBOutlet weak var stepButton: UIButton!
    
    var actionButtonDidTapped: (() -> Void)?
    
    func configure(slide: OnboardingSlide) {
        stepTitle.text = slide.title
        stepButton.setTitle(slide.buttonTitle, for: .normal)
        stepExplanation.text = slide.description
        stepButton.layer.cornerRadius = stepButton.layer.frame.height/4
        stepButton.layer.masksToBounds = true
        
        let animation = Animation.named(slide.animationName)
    
        animationView.animation = animation
        animationView.loopMode = .loop
        animationView.frame = self.bounds
        animationView.contentMode = .scaleAspectFit
        
        if !animationView.isAnimationPlaying {
            animationView.play()
        }
    }

    @IBAction func nextStepOnboarding(_ sender: Any) {
        actionButtonDidTapped?()
    }
}
