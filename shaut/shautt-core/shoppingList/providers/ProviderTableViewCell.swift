//
//  ProviderTableViewCell.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 31/08/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit

class ProviderTableViewCell: UITableViewCell {

    @IBOutlet weak var rowContentView: UIView!
    @IBOutlet weak var providerImageView1: UIImageView!
    @IBOutlet weak var providerImageView2: UIImageView!
    @IBOutlet weak var providerPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
