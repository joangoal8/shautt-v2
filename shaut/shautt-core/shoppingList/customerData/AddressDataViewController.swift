//
//  AddressDataViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 09/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit
import GooglePlaces
import Firebase
import Mixpanel

class AddressDataViewController: UIViewController {

    @IBOutlet weak var addressBtnContainer: UIView!
    @IBOutlet weak var houseAndDoorContainer: UIView!
    @IBOutlet weak var phoneContainer: UIView!
    
    @IBOutlet weak var addressBtn: UIButton!
    
    @IBOutlet weak var houseTextField: UITextField!
    @IBOutlet weak var doorTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    
    var interactor: AddressDataViewToPresenterInteractorProtocol?
    var addressUser: AddressUser?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        viewSetUp(view: addressBtnContainer)
        viewSetUp(view: houseAndDoorContainer)
        viewSetUp(view: phoneContainer)
        
        textFieldCurrentSetup(textField: houseTextField)
        textFieldCurrentSetup(textField: doorTextField)
        textFieldCurrentSetup(textField: phoneTextField)
        
        setUpInteractorWithAdressView()
    }
    
    fileprivate func viewSetUp(view: UIView) {
        view.layer.cornerRadius = 21
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        view.layer.shadowOpacity = 1
    }
    
    fileprivate func textFieldCurrentSetup(textField: UITextField) {
        textField.borderStyle = .none
    }
    
    fileprivate func setUpInteractorWithAdressView() {
        let addressViewInteractor = interactor as? MasterCartPresenterInteractor
        addressViewInteractor?.addressView = self
    }
    
    @IBAction func setStreet(_ sender: Any) {
        Mixpanel.mainInstance().track(event: ShauttConstants.ADD_ADDRESS_BY_GOOGLE)
        let autoCompleteController = GMSAutocompleteViewController()
        autoCompleteController.delegate = self
        self.present(autoCompleteController, animated: true, completion: nil)
    }
    
    @IBAction func okAddress(_ sender: Any) {
        if houseTextField.text == "" || doorTextField.text == "" || phoneTextField.text == "" {
            let message = "Todos los campos tienen que ser rellenados"
            displayRegisterAlert(title: "Error", message: message)
        } else {
            let regex = try? NSRegularExpression(pattern: "([+][0-9][0-9])?(([0-9]){9})", options: .caseInsensitive)
            if regex?.firstMatch(in: phoneTextField.text!, options: [], range: NSRange(location: 0, length: phoneTextField.text!.count)) == nil {
                let message = "Error con el número de teléfono proporcionado"
                displayRegisterAlert(title: "Error", message: message)
                return
            }
            if addressUser == nil {
                let message = "Error actualizando la dirección. Inténtelo de nuevo"
                displayRegisterAlert(title: "Error Direción", message: message)
                return
            }
            Analytics.logEvent(ShauttConstants.ADD_ADDRESS_TO_SHOPPING_CART, parameters: nil)
            if let address = addressUser {
                Mixpanel.mainInstance().track(event: ShauttConstants.CONFIRM_ADDRESS_SHOPPING_CART, properties: ["address": address.address, "city" : address.city, "postcode" : address.postalCode, "house" : houseTextField.text!, "door" : doorTextField.text!, "phone" : phoneTextField.text!])
                Mixpanel.mainInstance().people.set(properties: ["$city": address.city])
            }
            self.interactor?.storeAddresDetailsAndPhone(house: houseTextField.text!, door: doorTextField.text!, phone: phoneTextField.text!)
        }
    }
    
    fileprivate func displayRegisterAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
        }))
        Mixpanel.mainInstance().track(event: ShauttConstants.ERROR_FIELDS_ADDRESS_SHOPPING_CART)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func backToLastView(_ sender: Any) {
        Mixpanel.mainInstance().track(event: ShauttConstants.CLOSE_ADDING_ADDRESS)
        dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddressDataViewController: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.interactor?.processPlace(place: place)
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension AddressDataViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension AddressDataViewController: AddressDataPresenterToViewProtocol {
    
    func updateAddressStreet(addressUser: AddressUser) {
        DispatchQueue.main.async {
            self.addressUser = addressUser
            self.addressBtn.setTitle(addressUser.address, for: .normal)
            self.addressBtn.setTitleColor(.black, for: .normal)
        }
    }
    
    func addressUpdateDismiss() {
        dismiss(animated: true, completion: nil)
    }
    
}
