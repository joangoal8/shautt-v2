//
//  EditShoppingCartViewController.swift
//  shaut
//
//  Created by Joan Gómez Álvarez on 07/11/2020.
//  Copyright © 2020 Joan Gómez Álvarez. All rights reserved.
//

import UIKit
import Mixpanel

class EditShoppingCartViewController: UIViewController {

    @IBOutlet weak var shoppingCartProductsTableView: UITableView!

    var shoppingCartDto: ShoppingCartCandidateDto?
    var shoppingCartPresenterInteractor: EditShoppingCartViewToPresenterInteractorProtocol?
    
    var matrix = [Int: IndexPath]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func initMatrix() {
        guard let shoppingCart = shoppingCartDto else {
            return
        }
        
        for (index) in 0...shoppingCart.shoppingCartProducts.count - 1 {
            let selectItem = shoppingCart.shoppingCartProducts[index].selectedProduct
            let selectItemIndex = shoppingCart.shoppingCartProducts[index].otherOptionsProducts.firstIndex { (shoppingCartProduct) -> Bool in
                return selectItem.id == shoppingCartProduct.id
            }
            if let prodIndex = selectItemIndex {
                matrix[index] = IndexPath(row: prodIndex, section: 0)
            } else {
                matrix[index] = IndexPath(row: 0, section: 0)
            }
        }
    }
    
    fileprivate func isProductCellSelected(x: Int, y: Int, z: Int) -> Bool {
        guard let rowTable = matrix[x] else {
            return false
        }
        return rowTable.row == y && rowTable.section == z
    }
    
    private func updateShoppingCartWithSelection() {
        
        guard var shoppingCart = shoppingCartDto else {
            return
        }
        
        for index in 0...shoppingCart.shoppingCartProducts.count - 1 {
            if let optionIndex = matrix[index]?.row {
                shoppingCart.shoppingCartProducts[index].selectedProduct = shoppingCart.shoppingCartProducts[index].otherOptionsProducts[optionIndex]
            }
        }
        
        self.shoppingCartDto = shoppingCart
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func close(_ sender: Any) {
        Mixpanel.mainInstance().track(event: ShauttConstants.STOP_EDITING_ENTIRE_SHOPPING_CART)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func updateEntireShoppingCart(_ sender: Any) {
        
        updateShoppingCartWithSelection()
        guard let shoppingCart = shoppingCartDto else {
            dismiss(animated: true, completion: nil)
            return
        }
        let shoppingCartProductsText = shoppingCart.shoppingCartProducts.reduce( "") { (productsTxt, nextProduct) -> String in
            return "\(productsTxt), \(nextProduct.selectedProduct.name)"
        }
        Mixpanel.mainInstance().track(event: ShauttConstants.CONFIRM_UPDATE_ENTIRE_SHOPPING_CART, properties: ["shoppingCartProductsText": shoppingCartProductsText])
        shoppingCartPresenterInteractor?.updateShoppingCartProducts(shoppingCart: shoppingCart)
        dismiss(animated: true, completion: nil)
    }
    
}

extension EditShoppingCartViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let shoppingCart = shoppingCartDto else {
            return 1
        }
        return shoppingCart.shoppingCartProducts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let itemCell = tableView.dequeueReusableCell(withIdentifier: "EditShoppingCartProductType", for: indexPath) as! EditShoppingCartTableViewCell
        itemCell.setCollectionViewDataSourceDelegate(collectionDelegate: self, forRow: indexPath.row)
        
        return itemCell
    }
    
    /*
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let editCell = cell as! EditShoppingCartTableViewCell
        editCell.setCollectionViewDataSourceDelegate(collectionDelegate: self, forRow: indexPath.row)
    }
     */
}

extension EditShoppingCartViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let shoppingCart = shoppingCartDto else {
            return 1
        }
        return shoppingCart.shoppingCartProducts[collectionView.tag].otherOptionsProducts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let prodCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShoppingCartProductCell", for: indexPath) as! EditShoppingCartProductCollectionViewCell
        
        guard let shoppingCart = shoppingCartDto else {
            return prodCell
        }
        
        let shopProdType = shoppingCart.shoppingCartProducts[collectionView.tag]
        guard !shopProdType.otherOptionsProducts.isEmpty else {
            return prodCell
        }
        
        let isSelected = isProductCellSelected(x: collectionView.tag, y: indexPath.row, z: indexPath.section)
        prodCell.containerView.layer.borderColor = isSelected ? UIColor.systemPink.cgColor : UIColor.lightGray.cgColor
        prodCell.containerView.layer.borderWidth = isSelected ? 3 : 1
        prodCell.containerView.layer.cornerRadius = 21
        
        let product = shopProdType.otherOptionsProducts[indexPath.row]
        if let url = URL(string: product.imageUrl) {
            prodCell.shoppingCartProductImage.load(url: url)
        }
        prodCell.shoppingCartProductName.text = product.name
        
        prodCell.shoppingCartProductPrice.text = "\(product.price.round()) €"
        
        return prodCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let oldIndexPath = matrix[collectionView.tag] {
            collectionView.deselectItem(at: oldIndexPath, animated: true)
            if let oldProdCell = collectionView.cellForItem(at: oldIndexPath) {
                let oldEditCell = oldProdCell as! EditShoppingCartProductCollectionViewCell
                oldEditCell.containerView.layer.borderColor = UIColor.lightGray.cgColor
                oldEditCell.containerView.layer.borderWidth = 1
            }
        }
        matrix[collectionView.tag] = indexPath
        
        let prodCell = collectionView.cellForItem(at: indexPath) as! EditShoppingCartProductCollectionViewCell
        
        prodCell.containerView.layer.borderColor = UIColor.systemPink.cgColor
        prodCell.containerView.layer.borderWidth = 3
    }
}

